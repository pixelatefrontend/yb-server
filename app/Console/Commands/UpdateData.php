<?php

namespace YurtlarBurada\Console\Commands;

use Illuminate\Console\Command;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\Message;

class UpdateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Data updates and relations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = \DB::table('dorms')->select('id', 'old_id')->orderBy('old_id', 'DESC')->get();
        foreach($data as $row){
            \DB::table('dorm_faculty')->where('dorm_id', $row->old_id)->update([
                'dorm_id' => $row->id
            ]);
        }
    }
}
