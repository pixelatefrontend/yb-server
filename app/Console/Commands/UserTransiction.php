<?php

namespace YurtlarBurada\Console\Commands;

use Illuminate\Console\Command;

class UserTransiction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get User data from old Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
