<?php

namespace YurtlarBurada\Helpers;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class SmsApi {
    /**
     * @var Client API Client
     */
    protected $client;

    /**
     * Create new instance of Client
     */
    public function __construct() {
        $this->client = new Client([
            'timeout' => 10.0,
            'base_uri' => env('SMS_API_URL', 'http://gw.ekomesaj.com.tr/v1/json/syncreply/'),
        ]);
    }

    /**
     * @param $method
     * @param $endpoint
     * @param array $options
     *
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method, $endpoint, $options = []) {
        $options['headers'] = ['content-type' => 'application/json'];
        $options['body'] = \GuzzleHttp\json_encode($options['body']);
        try {
            $apiRequest = $this->client->request($method, $endpoint, $options);
            $response = $apiRequest->getBody()->getContents();
//            $response = \GuzzleHttp\json_decode($response);
            return $response;
        } catch (Exception $e) {
            Log::error($e);
            return false;
        }
    }

    /**
     * @param $message
     * @param $to
     * @param $sender
     *
     * @return bool|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function sendSMS($message, $to, $sender = 'yurtlarbrda') {
        $sms = new SmsApi();
        $opt['body'] = [
            "Credential" => [
                "Username" => env('SMS_API_USERNAME',''),
                "Password" => env('SMS_API_PASSWORD','')
            ],
            "DataCoding" => "Default",
            "Header" => [
                "From"=> $sender,
                "ValidityPeriod"=> 0
            ],
            "Message" => $message,
            "To" => $to
        ];
        return $sms->request('POST', 'Submit', $opt);
    }
}
