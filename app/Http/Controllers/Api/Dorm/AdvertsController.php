<?php

namespace YurtlarBurada\Http\Controllers\Api\Dorm;

use YurtlarBurada\Models\Advert;
use YurtlarBurada\Http\Controllers\Controller;

class AdvertsController extends Controller
{
    public function list()
    {
        $items = Advert::where('user_id', request()->user()->id)->orderBy('id', 'DESC')->get();
        return response()->json($items, 200);
    }

    public function detail($id)
    {
        $item = Advert::where('id', $id)->firstOrFail();
        if(!item) abort(404);
        return response()->json($item, 200);
    }

    public function create()
    {
        $table = new Advert;
        $table->user_id = request()->user()->id;
        $table->city_id = request('city_id');
        $table->category_id = request('category_id');
        $table->advertiser = request('advertiser');
        $table->title = request('title');
        $table->content = request('content');
        $table->web = request('web');
        $table->email = request('email');
        $table->phone = request('phone');
        $table->address = request('address');
        if($table->save()) {
            return response()->json($table, 200);
        }else {
            return response()->json([], 500);
        }
    }

    public function update($id)
    {
        $table = Advert::findOrFail($id);
        $table->city_id = request('city_id');
        $table->category_id = request('category_id');
        $table->advertiser = request('advertiser');
        $table->title = request('title');
        $table->content = request('content');
        $table->web = request('web');
        $table->email = request('email');
        $table->phone = request('phone');
        $table->address = request('address');
        if($table->save()) {
            return response()->json($table, 200);
        }else {
            return response()->json([], 500);
        }
    }
}
