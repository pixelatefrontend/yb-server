<?php

namespace YurtlarBurada\Http\Controllers\Api\Dorm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use YurtlarBurada\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Hash;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\User;
use DB;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = ['email' => $request->email, 'password' => $request->password, 'is_active' => 1, 'type_id' => 1];
        if (Auth::guard('web')->attempt($credentials)) {
            $user = Auth::guard('web')->user();

//            // Send an internal API request to get an access token
//            $client = DB::table('oauth_clients')
//                ->where('password_client', true)
//                ->first();
//
//            // Make sure a Password Client exists in the DB
//            if (!$client) {
//                return response()->json([
//                    'message' => 'Laravel Passport is not setup properly.',
//                    'status' => 500
//                ], 500);
//            }
//
//            $data = [
//                'grant_type' => 'password',
//                'client_id' => $client->id,
//                'client_secret' => $client->secret,
//                'username' => request('email'),
//                'email' => request('email'),
//                'password' => request('password'),
//            ];
//
//            $request = Request::create('/oauth/token', 'POST', $data);
//
//            $response = app()->handle($request);
//            // Check if the request was successful
//            if ($response->getStatusCode() != 200) {
//                return response()->json([
//                    'message' => "Token does not create for ID : {$user->id} - Email: {$user->email}",
//                    'status' => 401
//                ], 401);
//            }
//
//            // Get the data from the response
//            $data = json_decode($response->getContent());

            // Eger uyeye merkez yurt secilmemisse ilk yurdu merkez yurt olarak sec
            $dorm = Dorm::find($user->dorm_id) ?? Dorm::where('email', $user->email)->first();
            $user->dorm_name = $dorm->center_name ?? $dorm->name;
            $user->dorm_slug = $dorm->center_slug ?? $dorm->slug;
            $user->logo = $dorm->logo ?? null;

            // Format the final response in a desirable format
            return response()->json([
                'token' => $user->createToken('Laravel Personal Access Client')->accessToken,
                'user' => $user,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'message' => 'Wrong email or password!',
                'status' => 422
            ], 422);
        }


//        // Check if a user with the specified email exists
//        $user = User::whereEmail(request('email'))->first();
//
//        if (!$user) {
//            return response()->json([
//                'message' => 'Wrong email or password',
//                'status' => 422
//            ], 422);
//        }
//
//        // If a user with the email was found - check if the specified password
//        // belongs to this user
//        if (!Hash::check(request('password'), $user->password)) {
//            return response()->json([
//                'message' => 'Wrong email or password',
//                'status' => 422
//            ], 422);
//        }

//        // Send an internal API request to get an access token
//        $client = DB::table('oauth_clients')
//            ->where('password_client', true)
//            ->first();
//
//        // Make sure a Password Client exists in the DB
//        if (!$client) {
//            return response()->json([
//                'message' => 'Laravel Passport is not setup properly.',
//                'status' => 500
//            ], 500);
//        }
//
//        $data = [
//            'grant_type' => 'password',
//            'client_id' => $client->id,
//            'client_secret' => $client->secret,
//            'username' => request('email'),
//            'email' => request('email'),
//            'password' => request('password'),
//        ];
//
//        $request = Request::create('/oauth/token', 'POST', $data);
//
//        $response = app()->handle($request);
//
//        // Check if the request was successful
//        if ($response->getStatusCode() != 200) {
//            return response()->json([
//                'message' => 'Wrong email or password',
//                'status' => 422
//            ], 422);
//        }
//
//        // Get the data from the response
//        $data = json_decode($response->getContent());
//
//        // Format the final response in a desirable format
//        return response()->json([
//            'token' => $data->access_token,
//            'user' => $user,
//            'status' => 200
//        ]);
    }

    public function logout(Request $request)
    {
        $accessToken = auth()->user()->token();

        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        return response()->json(['status' => 200]);
    }
}
