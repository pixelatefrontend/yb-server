<?php

namespace YurtlarBurada\Http\Controllers\Api\Dorm;

use Illuminate\Http\Request;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\DormComment;
use YurtlarBurada\Http\Controllers\Controller;

class CommentsController extends Controller
{
    /**
     * @param $dorm_id
     * @return \Illuminate\Http\JsonResponse
     */

    public function list(Request $request)
    {
        $dorm_ids = Dorm::where('user_id', $request->user()->id)->select('id')->get();
        $ids = [];
        foreach ($dorm_ids as $row) {
            $ids[] = $row->id;
        }
        $items = DormComment::whereIn('dorm_id', $ids)->with('dorm', 'user')->orderBy('id', 'ASC')->get();
        return response()->json($items, 200);
    }

    public function detail($id)
    {
        $item = DormComment::where('id', $id)->firstOrFail();
        if(!$item) abort(404);
        return response()->json($item, 200);
    }
}
