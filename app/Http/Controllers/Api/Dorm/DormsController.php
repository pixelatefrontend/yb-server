<?php

namespace YurtlarBurada\Http\Controllers\Api\Dorm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use YurtlarBurada\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use YurtlarBurada\Models\DormImage as Gallery;
use YurtlarBurada\Models\Dorm;

class DormsController extends Controller
{
    public function list(Request $request)
    {
        $dorms = Dorm::where('user_id', $request->user()->id)->with('city:id,slug,name', 'district:id,slug,name', 'video:dorm_id,type,url')->get();
        return response()->json($dorms, 200);
    }

    public function get(Request $request, $dorm_id)
    {
        $dorm = Dorm::with('city:id,slug,name', 'district:id,slug,name', 'type', 'properties:id', 'user', 'images', 'faculties:id,name,university_id', 'faculties.university:id,name', 'video:dorm_id,type,url')->findOrFail($dorm_id);
        if (empty($dorm->user_id) || ($request->user()->id != $dorm->user_id)) { abort(403); }
        return $dorm;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'props' => 'array|min:1',
            'props.*' => 'string|distinct|min:1',
            'faculties' => 'array|min:1',
            'faculties.*' => 'string|distinct|min:1',
            'type_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'district_id' => 'required|numeric',
            'name' => 'required',
            'slug' => 'required',
            'keywords' => 'not_in:null,undefined',
            'description' => 'max:160|not_in:null,undefined',
            'member' => 'not_in:null,undefined',
            'capacity' => 'required|numeric|not_in:null,undefined',
            'is_wifi' => 'required|not_in:null,undefined',
            'meal' => 'not_in:null,undefined',
            'content' => 'not_in:null,undefined',
            'content_en' => 'not_in:null,undefined',
            'facilities' => 'not_in:null,undefined',
            'advantages' => 'not_in:null,undefined',
            'events' => 'not_in:null,undefined',
            'transportation' => 'not_in:null,undefined',
            'other' => 'not_in:null,undefined',
            'phone' => 'regex:/(0)[0-9]{10}/|not_in:null,undefined',
            'mobile_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
            'message_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
            'fax' => 'not_in:null,undefined',
            'email' => 'required|email',
            'web' => 'not_in:null,undefined',
            'address' => 'required|not_in:null,undefined',
            'lat' => 'not_in:null,undefined',
            'lng' => 'not_in:null,undefined',
            'note' => 'not_in:null,undefined',
        ]);

        $table = new Dorm;
        $table->user_id = $request->user()->id;
        $table->dorm_type_id = $request->type_id;
        $table->city_id = $request->city_id;
        $table->district_id = $request->district_id;
        $table->name = $request->name;
        $table->slug = $request->slug;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        $table->member = $request->member;
        $table->capacity = $request->capacity;
        $table->is_wifi = $request->is_wifi;
        $table->meal = $request->meal;
        $table->content = $request->input('content');
        $table->content_en = $request->content_en;
        $table->facilities = $request->facilities;
        $table->advantages = $request->advantages;
        $table->events = $request->events;
        $table->transportation = $request->transportation;
        $table->other = $request->other;
        $table->phone = $request->phone;
        $table->mobile_phone = $request->mobile_phone;
        $table->phone_for_message = $request->message_phone;
        $table->fax = $request->fax;
        $table->email = $request->email;
        $table->web = $request->web;
        $table->address = $request->address;
        $table->lat = $request->lat;
        $table->lng = $request->lng;
        $table->note = $request->note;
        if($table->save()){
            $panel = new \YurtlarBurada\Http\Controllers\Api\Panel\DormsController();
            if($request->has('props')) {
                $table->properties()->attach($request->props);
            }
            if($request->has('faculties')) {
                $table->faculties()->attach($request->faculties);
            }
            if($request->hasFile('images')) {
                $panel->imageUpload($request->images, $table->id, $table->slug);
            }
            if($request->hasFile('logo')) {
                $panel->logoUpload($request->logo, $table->id, $table->slug);
            }
            if($request->hasFile('cover')) {
                $panel->coverUpload($request->cover, $table->id, $table->slug);
            }
            return response()->json([ 'id' => $table->id ], 201);
        }else{
            return response()->json(['failed' => true], 403);
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'props' => 'array|min:1',
            'props.*' => 'string|distinct|min:1',
            'faculties' => 'array|min:1',
            'faculties.*' => 'string|distinct|min:1',
            'type_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'district_id' => 'required|numeric',
            'name' => 'required',
            'slug' => 'required',
            'keywords' => 'not_in:null,undefined',
            'description' => 'max:160|not_in:null,undefined',
            'member' => 'not_in:null,undefined',
            'capacity' => 'required|numeric|not_in:null,undefined',
            'is_wifi' => 'required|not_in:null,undefined',
            'meal' => 'not_in:null,undefined',
            'content' => 'not_in:null,undefined',
            'content_en' => 'not_in:null,undefined',
            'facilities' => 'not_in:null,undefined',
            'advantages' => 'not_in:null,undefined',
            'events' => 'not_in:null,undefined',
            'transportation' => 'not_in:null,undefined',
            'other' => 'not_in:null,undefined',
            'phone' => 'regex:/(0)[0-9]{10}/|not_in:null,undefined',
            'mobile_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
            'message_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
            'fax' => 'not_in:null,undefined',
            'email' => 'required|email',
            'web' => 'not_in:null,undefined',
            'address' => 'required|not_in:null,undefined',
            'lat' => 'not_in:null,undefined',
            'lng' => 'not_in:null,undefined',
            'note' => 'not_in:null,undefined',
        ]);

        $table = Dorm::findOrFail($id);
        if (empty($table->user_id) || ($request->user()->id != $table->user_id)) { abort(403); }
        $table->user_id = $request->user()->id;
        $table->dorm_type_id = $request->type_id;
        $table->city_id = $request->city_id;
        $table->district_id = $request->district_id;
        $table->name = $request->name;
        $table->slug = $request->slug;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        $table->member = $request->member;
        $table->capacity = $request->capacity;
        $table->is_wifi = $request->is_wifi;
        $table->meal = $request->meal;
        $table->content = $request->input('content');
        $table->content_en = $request->content_en;
        $table->facilities = $request->facilities;
        $table->advantages = $request->advantages;
        $table->events = $request->events;
        $table->transportation = $request->transportation;
        $table->other = $request->other;
        $table->phone = $request->phone;
        $table->mobile_phone = $request->mobile_phone;
        $table->phone_for_message = $request->message_phone;
        $table->fax = $request->fax;
        $table->email = $request->email;
        $table->web = $request->web;
        $table->address = $request->address;
        $table->lat = $request->lat;
        $table->lng = $request->lng;
        $table->note = $request->note;
        $table->virtual_tour = $request->virtual_tour;
        if($table->save()){
            $panel = new \YurtlarBurada\Http\Controllers\Api\Panel\DormsController();
            if (!empty($request->video_url) && !empty($request->video_type)) {
                $table->video()->updateOrCreate(
                    [ 'dorm_id' => $table->id ],
                    [
                        'url' => $request->video_url,
                        'type' => $request->video_type,
                    ]
                );
            }
            if($request->hasFile('logo')) {
                $panel->logoUpload($request->logo, $id, $table->slug);
            }
            if($request->hasFile('cover')) {
                $panel->coverUpload($request->cover, $id, $table->slug);
            }
            if($request->hasFile('images')) {
                $panel->imageUpload($request->images, $id, $table->slug);
            }
            if($request->has('props')) {
                $table->properties()->sync($request->props);
            }
            if($request->has('faculties')) {
                $table->faculties()->sync($request->faculties);
            }
            return response()->json([ 'id' => $table->id ], 200);
        }else{
            return response()->json(['failed' => true], 403);
        }
    }

    public function imageDestroy(Request $request, $id)
    {
        if($id) {
            $item = Gallery::findOrFail($id);
            try {
                Storage::disk('spaces')->delete('/uploads/dorms/'.$item->dorm->id.'/'.$item->getImage());
                $item->delete();
                return response()->json([], 200);
            }catch(\Exception $e) {
                return response()->json($e, $e->getStatusCode());
            }
        }
    }

    public function coverDestroy($id)
    {
        $table = Dorm::findOrFail($id);
        Storage::disk('spaces')->delete('uploads/dorms/'.$id.'/'.$table->getCover());
        $table->cover = null;
        $table->save();
        return response()->json([], 200);
    }

    public function logoDestroy($id)
    {
        $table = Dorm::findOrFail($id);
        Storage::disk('spaces')->delete('uploads/dorms/'.$id.'/'.$table->getLogo());
        $table->logo = null;
        $table->save();
        return response()->json([], 200);
    }
}
