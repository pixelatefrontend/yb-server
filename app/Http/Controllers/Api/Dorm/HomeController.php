<?php

namespace YurtlarBurada\Http\Controllers\Api\Dorm;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index() 
    {
        return ':)';
    }
}
