<?php

namespace YurtlarBurada\Http\Controllers\Api\Dorm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\User;

class InfoController extends Controller
{
    public function get()
    {
        $user = request()->user();
        // Eger uyeye merkez yurt secilmemisse ilk yurdu merkez yurt olarak sec
        $dorm = Dorm::find($user->dorm_id) ?? Dorm::where('email', $user->email)->first();

        $user->center_name = $dorm->center_name ?? null;
        $user->center_slug = $dorm->center_slug ?? null;
        $user->logo = $dorm->logo ?? null;
        return response()->json($user, 200);
    }

    public function updateInfo()
    {
        $this->validate(request(), [
           'first_name' => 'required',
           'last_name' => 'required',
           'phone' => 'required|regex:/(0)[0-9]{10}/',
           'email' => 'required|email',
        ]);

        $id = request()->user()->id;
        $user = User::findOrFail($id);
        $user->first_name = request('first_name');
        $user->last_name = request('last_name');
        $user->phone = request('phone');
        $user->email = request('email');

        // Eger uyeye merkez yurt secilmemisse ilk yurdu merkez yurt olarak sec
        $dorm = Dorm::find($user->dorm_id) ?? Dorm::where('email', $user->email)->first();

        if ($dorm->id) {
            if (request()->has('center_name')) {
                $dorm->center_name = request('center_name');
                $dorm->center_slug = str_slug(request('center_name'),'-');
                $dorm->save();
            }
            if(request()->hasFile('logo')) {
                $panel = new \YurtlarBurada\Http\Controllers\Api\Panel\DormsController();
                $panel->logoUpload(request()->logo, $dorm->id, $dorm->slug);
            }
            // Eger uyeye merkez yurt secilmemisse ilk yurdu merkez yurt olarak tanimla
            $user->dorm_id = $user->dorm_id ?? $dorm->id;
        }

        if($user->save()){
            return response()->json([], 200);
        }
    }

    public function updatePassword(Request $request) {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
            'old_password' => 'required|min:6',
        ]);

        if (Hash::check($request->old_password, request()->user()->password)) {
            $user = User::findOrFail(request()->user()->id);
            $user->password = bcrypt(request('password'));
            $user->save();
            return response()->json(['message' => 'Basarili!'], 200);
        } else {
            return response()->json(['errors' => ['old_password'=>['Eski şifre eşleşmiyor!']]], 422);
        }
    }
}
