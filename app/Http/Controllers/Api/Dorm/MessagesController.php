<?php

namespace YurtlarBurada\Http\Controllers\Api\Dorm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\DormMessage;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\User;

class MessagesController extends Controller
{
    public function list(Request $request)
    {

        $dorm_ids = Dorm::where('user_id', $request->user()->id)->select('id')->get();
        $ids = [];
        foreach ($dorm_ids as $row) {
            $ids[] = $row->id;
        }
        $items = DormMessage::whereIn('dorm_id', $ids)->with('dorm')->orderBy('id', 'ASC')->get();
        return response()->json($items, 200);
    }

    public function detail($id)
    {
        $item = DormMessage::where('id', $id)->with('dorm')->firstOrFail();
        if(!$item) abort(404);
        return response()->json($item, 200);
    }
}
