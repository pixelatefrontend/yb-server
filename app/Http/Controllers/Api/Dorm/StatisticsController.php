<?php

namespace YurtlarBurada\Http\Controllers\Api\Dorm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Advert;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\DormComment;
use YurtlarBurada\Models\DormMessage;
use YurtlarBurada\Models\DormPhoneClick;
use YurtlarBurada\Models\Favorite;

class StatisticsController extends Controller
{

    function getStatistics() {
        $user = Auth::user();

        $dorms = Dorm::where('user_id', $user->id);
        $dorm_ids = $dorms->pluck('id')->toArray();

        $dormComments = DormComment::whereIn('dorm_id', $dorm_ids);
        $dormMessages = DormMessage::whereIn('dorm_id', $dorm_ids);
        $dormFavorites = Favorite::whereIn('dorm_id', $dorm_ids);
        $dormClicksWhatsapp = DormPhoneClick::where('type', 'whatsapp')->whereIn('dorm_id', $dorm_ids)->get();
        $dormClickNormal = DormPhoneClick::where('type', 'normal')->whereIn('dorm_id', $dorm_ids)->get();
        $adverts = Advert::where('user_id', $user->id)->get();

        $response['user'] = $user;
        $response['dorms'] = $dorms->select('id','name', 'slug')->get();
        $response['counts'] = [
            'views' => (int) $dorms->sum('number_of_views') ?? 0,
            'comments' => $dormComments->count() ?? 0,
            'messages' => $dormMessages->count() ?? 0,
            'favorites' => $dormFavorites->count() ?? 0,
            'dorms' => $dorms->count() ?? 0,
            'adverts' => $adverts->count() ?? 0,
            'normalClicks' => $dormClickNormal,
            'whatsappClicks' => $dormClicksWhatsapp,
        ];

        return response()->json($response, 200);
    }

    function getDormStatistics($dorm_id) {
        $user = Auth::user();

        $dorm = Dorm::find($dorm_id);

        $dormComments = DormComment::where('dorm_id', $dorm_id);
        $dormMessages = DormMessage::where('dorm_id', $dorm_id);
        $dormFavorites = Favorite::where('dorm_id', $dorm_id);
        $adverts = Advert::where('user_id', $user->id)->get();

        $response['counts'] = [
            'views' => $dorm->number_of_views ?? 0,
            'comments' => $dormComments->count() ?? 0,
            'messages' => $dormMessages->count() ?? 0,
            'likes' => $dorm->number_of_likes ?? 0,
            'favorites' => $dormFavorites->count() ?? 0,
        ];

        return response()->json($response, 200);
    }


}
