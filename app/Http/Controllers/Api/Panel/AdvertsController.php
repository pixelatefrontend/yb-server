<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Advert;
use YurtlarBurada\Models\AdvertCategory as Category;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\AdvertImage;
use Intervention\Image\ImageManagerStatic as Image;


class AdvertsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $request->input('title');
        $category = $request->input('category');

        $adverts = Advert::with('category')
            ->when($title, function ($query) use ($title) {
                return $query->where('title', 'like', '%'.$title.'%');
            })
            ->when($category, function ($query) use ($category) {
                return $query->where('category_id', $category);
            })
            ->orderBy('id','DESC')
            ->paginate(20);

        $categories = Category::all();

        $response = [
            'pagination' => [
                'total' => $adverts->total(),
                'per_page' => $adverts->perPage(),
                'current_page' => $adverts->currentPage(),
                'last_page' => $adverts->lastPage(),
                'from' => $adverts->firstItem(),
                'to' => $adverts->lastItem()
            ],
            'data' => $adverts,
            'categories' => $categories
        ];
        return response()->json($response, 200);
    }

    public function create()
    {
        $data = [ 'cities' => City::all(), 'categories' => Category::all() ];
        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new Advert;
        $table->category_id = $request->category;
        $table->city_id = $request->city;
        $table->cover = $request->cover;
        $table->advertiser = $request->advertiser;
        $table->title = $request->title;
        $table->content = $request->input('content');
        $table->web = $request->web;
        $table->email = $request->email;
        $table->phone = $request->phone;
        $table->address = $request->address;
        if($table->save()) {
            if($request->hasFile('images')) {
                $images = $request->images;
                foreach($images as $image) {
                    $orijinalImage = $image;
                    $image = Image::make($orijinalImage);
                    $path = public_path().'/uploads/adverts/'.$table->id.'/';
                    $fileName = time().rand().'.'.$orijinalImage->getClientOriginalExtension();
                    \File::exists($path) or \File::makeDirectory($path,0777,true);
                    $image->save($path . $fileName);
                    $model = new AdvertImage;
                    $model->advert_id = $table->id;
                    $model->filename = $fileName;
                    $model->save();
                }
            }
            return response()->json($table, 201);
        }else {
            return response()->json([], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $cities = City::all();
        $categories = Category::all();
        $advert = Advert::with('category', 'city', 'images')->findOrFail($id);
        $data = [ 'cities' => $cities, 'categories' => $categories, 'advert' => $advert ];
        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('images')) {
            $images = $request->images;
            foreach($images as $image) {
                $orijinalImage = $image;
                $image = Image::make($orijinalImage);
                $path = public_path().'/uploads/adverts/'.$id.'/';
                $fileName = time().rand().'.'.$orijinalImage->getClientOriginalExtension();
                \File::exists($path) or \File::makeDirectory($path,0777,true);
                $image->save($path . $fileName);
                $model = new AdvertImage;
                $model->advert_id = $id;
                $model->filename = $fileName;
                $model->save();
            }
        }
        $table = Advert::findOrFail($id);
        $table->category_id = $request->category;
        $table->city_id = $request->city;
        $table->cover = $request->cover;
        $table->advertiser = $request->advertiser;
        $table->title = $request->title;
        $table->content = $request->input('content');
        $table->web = $request->web;
        $table->email = $request->email;
        $table->phone = $request->phone;
        $table->address = $request->address;
        $table->save();
        return response()->json(['success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Advert::findOrFail($id)->delete();
        return response()->json([], 200);
    }
}
