<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use DB;



class AuthController extends Controller
{
    public function login(Request $request)
    {
        $email = $request->email ?? $request->username;
        $credentials = ['email' => $email, 'password' => $request->password, 'is_active' => 1, 'type_id' => 3];
        if (Auth::guard('web')->attempt($credentials)) {

            // Send an internal API request to get an access token
            $client = DB::table('oauth_clients')
                ->where('password_client', true)
                ->first();

            // Make sure a Password Client exists in the DB
            if (!$client) {
                return response()->json([
                    'message' => 'Laravel Passport is not setup properly.',
                    'status' => 500
                ], 500);
            }

            $data = [
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'username' => $email,
                'email' => $email,
                'password' => request('password'),
            ];

            $request = Request::create('/oauth/token', 'POST', $data);

            $response = app()->handle($request);
            // Check if the request was successful
            if ($response->getStatusCode() != 200) {
                return response()->json([
                    'message' => 'Wrong email or password',
                    'status' => 422
                ], 422);
            }

            // Get the data from the response
            $data = json_decode($response->getContent());

            // Format the final response in a desirable format
            return response()->json([
                'token' => $data->access_token,
                'user' => Auth::guard('web')->user(),
                'status' => 200
            ]);
        } else {
            return response()->json([
                'message' => 'Wrong email or password',
                'status' => 422
            ], 422);
        }
//        // Check if a user with the specified email exists
//        $user = User::whereEmail(request('username'))->first();
//
//        if (!$user) {
//            return response()->json([
//                'message' => 'Wrong email or password',
//                'status' => 422
//            ], 422);
//        }
//
//        // If a user with the email was found - check if the specified password
//        // belongs to this user
//        if (!Hash::check(request('password'), $user->password)) {
//            return response()->json([
//                'message' => 'Wrong email or password',
//                'status' => 422
//            ], 422);
//        }
//
//        // Send an internal API request to get an access token
//        $client = DB::table('oauth_clients')
//            ->where('password_client', true)
//            ->first();
//
//        // Make sure a Password Client exists in the DB
//        if (!$client) {
//            return response()->json([
//                'message' => 'Laravel Passport is not setup properly.',
//                'status' => 500
//            ], 500);
//        }
//
//        $data = [
//            'grant_type' => 'password',
//            'client_id' => $client->id,
//            'client_secret' => $client->secret,
//            'username' => request('username'),
//            'password' => request('password'),
//        ];
//
//        $request = Request::create('/oauth/token', 'POST', $data);
//
//        $response = app()->handle($request);
//
//        // Check if the request was successful
//        if ($response->getStatusCode() != 200) {
//            return response()->json([
//                'message' => 'Wrong email or password',
//                'status' => 422
//            ], 422);
//        }
//
//        // Get the data from the response
//        $data = json_decode($response->getContent());
//
//        // Format the final response in a desirable format
//        return response()->json([
//            'token' => $data->access_token,
//            'user' => $user,
//            'status' => 200
//        ]);
    }

    public function logout(Request $request)
    {
        $accessToken = auth()->user()->token();

        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        return response()->json(['status' => 200]);
    }
}
