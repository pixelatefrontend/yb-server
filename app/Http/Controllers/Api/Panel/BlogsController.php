<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Blog;
use YurtlarBurada\Models\BlogCategory;
use YurtlarBurada\Models\BlogImage as Gallery;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;



class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::with('category')->get();
        $blogCategories = BlogCategory::all();
        $data = [
            'blogs' => $blogs,
            'blogCategories' => $blogCategories
        ];
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = BlogCategory::all();
        return response()->json($categories, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
        ]);

        $table = new Blog;
        $table->user_id = 5438;
        $table->category_id = $request->category;
        $table->title = $request->title;
        $table->slug = $request->slug;
        $table->content = request('content');
        $table->meta_keywords = $request->meta_keywords;
        $table->meta_description = $request->meta_description;
        $table->is_constant = $request->is_constant;
        if($table->save()){
            if($request->hasFile('cover')) {
                $this->coverUpload($request->cover, $table->id);
            }
            if($request->hasFile('images')) {
                $this->galleryUpload($request->images, $table->id);
            }
            return response()->json($table, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Blog::with('category', 'images')->findOrFail($id);
        $categories = BlogCategory::all();
        $data = [ 'item' => $item, 'categories' => $categories ];
        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
        ]);

        $table = Blog::findOrFail($id);
        $table->category_id = $request->category;
        $table->title = $request->title;
        $table->slug = $request->slug;
        $table->content = request('content');
        $table->meta_keywords = $request->meta_keywords;
        $table->meta_description = $request->meta_description;
        $table->is_constant = $request->is_constant;
        if($table->save()){
            if($request->hasFile('cover')) {
                $this->coverUpload($request->cover, $id);
            }
            if($request->hasFile('images')) {
                $this->galleryUpload($request->images, $id);
            }
            return response()->json($table, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blog::findOrFail($id)->delete();
        return response()->json([], 200);
    }

    public function coverUpload($image, $id)
    {
        $fileName = time().rand().'.'.$image->extension();
        $path = Storage::disk('spaces')->putFileAs('uploads/blogs/'.$id.'/', $image, $fileName, 'public');
        $table = Blog::findOrFail($id);
        $table->cover = $fileName;
        $table->save();
    }

    public function galleryUpload($images, $id)
    {
        foreach($images as $key => $image):
            $extension = $image->extension();
            $fileName = str_random() . uniqid().'.'.$extension;
            $path = 'uploads/blogs/'.$id.'/'.$fileName;
            $image = Image::make($image);
            if($image->height() > 800) {
                $image->resize(800, null, function($constraint) {
                    $constraint->aspectRatio();
                });
            }
            Storage::disk('spaces')->put($path, (string) $image->encode(), 'public');

            $table = new Gallery;
            $table->blog_id = $id;
            $table->filename = $fileName;
            $table->save();
        endforeach;
    }

    public function imageDestroy(Request $request, $id)
    {
        if($id) {
            $item = Gallery::with('blog')->findOrFail($id);
            try {
                Storage::disk('spaces')->delete('/uploads/blogs/'.$item->blog->id.'/'.$item->getImage());
                $item->delete();
                return response()->json([], 200);
            }catch(\Exception $e) {
                return response()->json($e, $e->getStatusCode());
            }
        }
    }

    public function coverDestroy($id)
    {
        $table = Blog::findOrFail($id);
        Storage::disk('spaces')->delete('uploads/blogs/'.$id.'/'.$table->getCover());
        $table->cover = null;
        $table->save();
        return response()->json([], 200);
    }


}
