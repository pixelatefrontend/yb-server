<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\CityImage as Gallery;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;


class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        return response()->json($cities, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new City;
        $table->image = $request->image;
        $table->icon = $request->icon;
        $table->name = $request->name;
        $table->slug = $request->slug;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        $table->detail_description = $request->detail_description;
        $table->detail_spot = $request->detail_spot;
        $table->detail_keywords = $request->detail_keywords;
        if($table->save()) {
            return response()->json($table, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = City::with('images')->findOrFail($id);
        return response()->json($item, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = City::findOrFail($id);
        $table->name = $request->name;
        $table->slug = $request->slug;
        $table->title = $request->title;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        $table->spot = $request->spot;
        $table->list_seo = $request->list_seo;
        $table->content = $request->input('content');
        $table->detail_title = $request->detail_title;
        $table->detail_description = $request->detail_description;
        $table->detail_spot = $request->detail_spot;
        $table->detail_keywords = $request->detail_keywords;
        $table->male_title = $request->male_title;
        $table->male_keywords = $request->male_keywords;
        $table->male_description = $request->male_description;
        $table->male_spot = $request->male_spot;
        $table->female_title = $request->female_title;
        $table->female_keywords = $request->female_keywords;
        $table->female_description = $request->female_description;
        $table->female_spot = $request->female_spot;

        if($request->hasFile('image')) {
            $this->imageUpload($request->file('image'), $id);
        }
        if($request->hasFile('icon')) {
            $this->iconUpload($request->file('icon'), $id);
        }
        if($request->hasFile('images')) {
            $this->galleryUpload($request->images, $id);
        }
        if($table->save()) {
            return response()->json([], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        City::findOrFail($id)->delete();
        return repsonse()->json([], 200);
    }

    public function imageUpload($image, $id)
    {
        $fileName = time().rand().'.'.$image->extension();
        $path = Storage::disk('spaces')->putFileAs('uploads/cities/'.$id.'/', $image, $fileName, 'public');
        $table = City::findOrFail($id);
        $table->image = $fileName;
        $table->save();
    }

    public function iconUpload($image, $id)
    {
        $fileName = time().rand().'.'.$image->extension();
        $path = Storage::disk('spaces')->putFileAs('uploads/cities/'.$id.'/', $image, $fileName, 'public');
        $table = City::findOrFail($id);
        $table->icon = $fileName;
        $table->save();
    }

    public function galleryUpload($images, $id)
    {
        foreach($images as $key => $image):
            $extension = $image->extension();
            $fileName = str_random() . uniqid().'.'.$extension;
            $path = 'uploads/cities/'.$id.'/'.$fileName;
            $image = Image::make($image);
            if($image->height() > 800) {
                $image->resize(800, null, function($constraint) {
                    $constraint->aspectRatio();
                });
            }
            Storage::disk('spaces')->put($path, (string) $image->encode(), 'public');

            $table = new Gallery;
            $table->city_id = $id;
            $table->filename = $fileName;
            $table->save();
        endforeach;
    }

    public function imageDestroy(Request $request, $id)
    {
        if($id) {
            $item = Gallery::with('city')->findOrFail($id);
            try {
                Storage::disk('spaces')->delete('/uploads/cities/'.$item->city->id.'/'.$item->getImage());
                $item->delete();
                return response()->json([], 200);
            }catch(\Exception $e) {
                return response()->json($e, $e->getStatusCode());
            }
        }
    }

    public function coverDestroy($id)
    {
        $table = City::findOrFail($id);
        Storage::disk('spaces')->delete('uploads/cities/'.$id.'/'.$table->getImage());
        $table->image = null;
        $table->save();
        return response()->json([], 200);
    }

    public function iconDestroy($id)
    {
        $table = City::findOrFail($id);
        Storage::disk('spaces')->delete('uploads/cities/'.$id.'/'.$table->getIcon());
        $table->icon = null;
        $table->save();
        return response()->json([], 200);
    }
}
