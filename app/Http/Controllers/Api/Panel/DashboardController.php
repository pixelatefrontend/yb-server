<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\User;
use YurtlarBurada\Models\University;
use YurtlarBurada\Models\Advert;
use YurtlarBurada\Models\Promote;
use YurtlarBurada\Models\Page;
use YurtlarBurada\Models\Message;
use YurtlarBurada\Models\Contact;
use YurtlarBurada\Models\News;
use YurtlarBurada\Models\CityComment;

class DashboardController extends Controller
{
    public function index()
    {
        $dorms = Dorm::all()->count();
        $users = User::all()->count();
        $universities = University::all()->count();
        $adverts = Advert::all()->count();
        $pages = Page::all()->count();
        $promotes = Promote::all()->count();
        $messages = Message::all()->count();
        $news = News::all()->count();
        $contacts = Contact::all()->count();
        $contact_forms = Contact::where('is_read', 0)->get();
        $city_comments = CityComment::where('is_active', 0)->with('city')->get();
        $popular_dorms = Dorm::orderBy('number_of_views', 'DESC')->with('city', 'district')->take(20)->get();
        return response()->json([
        	'dorms' => $dorms,
        	'users' => $users,
        	'universities' => $universities,
        	'adverts' => $adverts,
        	'promotes' => $promotes,
            'pages' => $pages,
            'messages' => $messages,
            'news' => $news,
            'contacts' => $contacts,
            'contact_forms' => $contact_forms,
            'city_comments' => $city_comments,
            'popular_dorms' => $popular_dorms,
        ], 200);
    }
}
