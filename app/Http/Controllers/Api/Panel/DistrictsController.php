<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\District;
use YurtlarBurada\Models\City;


class DistrictsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = City::all();
        $districts = District::where('city_id', 1)->with('city')->get();
        if($request->filled('city')) {
            $districts = District::where('city_id', $request->city)->with('city')->get();
        }
        return response()->json([ 'cities' => $cities, 'districts' => $districts ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        return response()->json($cities, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new District;
        $table->city_id = $request->city;
        $table->name = $request->name;
        $table->slug = $request->slug;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        if($table->save()) {
            return response()->json($table, 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cities = City::all();
        $table = District::findOrFail($id);
        return response()->json([ 'cities' => $cities, 'district' => $table ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = District::findOrFail($id);
        $table->name = $request->name;
        $table->title = $request->title;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        $table->spot = $request->spot;
        $table->list_seo = $request->list_seo;
        $table->male_title = $request->male_title;
        $table->male_keywords = $request->male_keywords;
        $table->male_description = $request->male_description;
        $table->male_spot = $request->male_spot;
        $table->female_title = $request->female_title;
        $table->female_keywords = $request->female_keywords;
        $table->female_description = $request->female_description;
        $table->female_spot = $request->female_spot;
        if($table->save()) {
            return response()->json([], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        District::findOrFail($id)->delete();
        return response()->json([], 200);
    }
}
