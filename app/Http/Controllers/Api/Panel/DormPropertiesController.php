<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\PropertyCategory as PC;
use YurtlarBurada\Models\DormProperty as DM;

class DormPropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PC::with('properties')->get();
        return response()->json($data, 200);
    }

    public function getList()
    {
        $data = DM::with('category')->orderBy('title', 'ASC')->get();
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = PC::all();
        return response()->json($categories, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new DM;
        $table->category_id = $request->category;
        $table->title = $request->title;
        $table->slug = $request->slug;
        if($table->save()) {
            return response()->json($table, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = DM::with('category')->findOrFail($id);
        $categories = PC::all();
        return response()->json([ 'data' => $item, 'categories' => $categories], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = DM::findOrFail($id);
        $table->category_id = $request->category;
        $table->title = $request->title;
        $table->slug = $request->slug;
        if($table->save()) {
            return response()->json($table, 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DM::findOrFail($id)->delete();
        return response()->json([], 200);
    }
}
