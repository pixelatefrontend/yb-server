<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\DormRoomImage;
use YurtlarBurada\Models\DormRoom;


class DormRoomsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $req
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $req)
    {
        $rooms = DormRoom::with(['dorm'])->orderBy('id', 'DESC');
        if ($req->dorm_id) {
            $rooms->where('dorm_id', $req->dorm_id);
        }
        if ($req->title) {
            $rooms->where('title', 'like', '%'.$req->title.'%');
        }
        if ($req->is_sale_active) {
            $rooms->where('is_sale_active', $req->is_sale_active);
        }
        if ($req->capacity) {
            $rooms->where('capacity', $req->capacity);
        }
        $response = $rooms
            ->paginate(50);

        return response()->json($response);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'props' => 'array|min:1',
            'props.*' => 'string|distinct|min:1',
            'dorm_id' => 'required|numeric',
            'title' => 'required',
            'capacity' => 'required|numeric',
            'daily_price' => 'numeric',
            'monthly_price' => 'numeric',
            'number' => 'not_in:null,undefined',
            'size' => 'not_in:null,undefined',
            'is_sale_active' => 'required|numeric',
            'is_active' => 'required|numeric',
        ]);

        $table = new DormRoom();
        $table->dorm_id = $request->dorm_id;
        $table->title = $request->title;
        $table->capacity = $request->capacity;
        $table->daily_price = $request->daily_price;
        $table->monthly_price = $request->monthly_price;
        $table->number = $request->number;
        $table->size = $request->size;
        $table->sort = $request->sort;
        $table->is_active = $request->is_active;
        $table->is_sale_active = $request->is_sale_active;
        if($table->save()) {
            if($request->has('props')) {
                $table->properties()->attach($request->props);
            }
            if($request->hasFile('images')) {
                $this->imageUpload($request->images, $table->id, str_slug($table->title));
            }
            return response()->json($table, 201);
        }else {
            return response()->json([ 'failed' => true ], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $message = DormRoom::with('dorm')->findOrFail($id);
        return response()->json($message, 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'props' => 'array|min:1',
            'props.*' => 'string|distinct|min:1',
            'dorm_id' => 'required|numeric',
            'title' => 'required',
            'capacity' => 'required|numeric',
            'daily_price' => 'numeric',
            'monthly_price' => 'numeric',
            'number' => 'not_in:null,undefined',
            'size' => 'not_in:null,undefined',
            'is_sale_active' => 'required|numeric',
            'is_active' => 'required|numeric',
        ]);

        $table = DormRoom::findOrFail($id);
        $table->dorm_id = $request->dorm_id;
        $table->title = $request->title;
        $table->capacity = $request->capacity;
        $table->daily_price = $request->daily_price;
        $table->monthly_price = $request->monthly_price;
        $table->number = $request->number;
        $table->size = $request->size;
        $table->sort = $request->sort;
        $table->is_active = $request->is_active;
        $table->is_sale_active = $request->is_sale_active;
        if($table->save()) {
            if($request->has('props')) {
                $table->properties()->attach($request->props);
            }
            if($request->hasFile('images')) {
                $this->imageUpload($request->images, $table->id, str_slug($table->title));
            }
            return response()->json($table, 200);
        }else {
            return response()->json([ 'failed' => true ], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        DormRoom::findOrFail($id)->delete();
        return response()->json([], 200);
    }



    public function imageUpload($images, $id, $slug)
    {
        foreach($images as $key => $image):
            $extension = $image->extension();
            $fileName = $slug.'-'.uniqid().'.'.$extension;
            $path = 'uploads/dorms/'.$id.'/'.$fileName;
            $image = Image::make($image);
            if($image->height() > 800) {
                $image->resize(800, null, function($constraint) {
                    $constraint->aspectRatio();
                });
            }
            Storage::disk('spaces')->put($path, (string) $image->encode(), 'public');

            $table = new DormRoomImage();
            $table->dorm_room_id = $id;
            $table->filename = $fileName;
            $table->save();
        endforeach;
    }

    public function imageDestroy(Request $request, $id)
    {
        if($id) {
            $item = DormRoomImage::findOrFail($id);
            try {
                Storage::disk('spaces')->delete('/uploads/dorms/'.$item->dorm->id.'/'.$item->getImage());
                $item->delete();
                return response()->json([], 200);
            }catch(\Exception $e) {
                return response()->json($e, $e->getStatusCode());
            }
        }
    }

}
