<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use YurtlarBurada\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\User;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\PropertyCategory as PC;
use YurtlarBurada\Models\DormImage as Gallery;
use YurtlarBurada\Models\Faculty;
use Illuminate\Support\Facades\Storage;


class DormsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->input('name');
        $city = $request->input('city');
        $district = $request->input('district');
        $type = $request->input('type');
        $status = $request->input('status');

        $dorms = Dorm::with('city', 'district', 'type')
            ->when($name, function ($query) use ($name) {
                return $query->where('name', 'like', '%'.$name.'%');
            })
            ->when($city, function ($query) use ($city) {
                return $query->where('city_id', $city);
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('district_id', $district);
            })
            ->when($type, function ($query) use ($type) {
                return $query->where('dorm_type_id', $type);
            })
            ->when($request->filled('status'), function ($query) use ($status) {
                return $query->where('is_active', $status);
            })
            ->when(!$request->filled('status'), function ($query) {
                return $query->where('is_active', 1);
            })
            ->orderBy('id','DESC')
            ->paginate(50);

        $response = [
            'request' => $request->all(),
            'pagination' => [
                'total' => $dorms->total(),
                'per_page' => $dorms->perPage(),
                'current_page' => $dorms->currentPage(),
                'last_page' => $dorms->lastPage(),
                'from' => $dorms->firstItem(),
                'to' => $dorms->lastItem()
            ],
            'data' => $dorms
        ];
        return response()->json($response, 200);
    }

    public function create()
    {
        $users = User::select('id', 'first_name', 'last_name')->where('type_id', 1)->get();
        $data = [];
        foreach($users as $user) {
            $data[] = [ 'value' => $user->id, 'text' => $user->first_name .' '. $user->last_name ];
        }
        $cities = City::select('id', 'name')->get();
        $properties = PC::with('properties')->get()->toArray();
        $universities = Faculty::select('id', 'name', 'university_id')->with('university:id,name')->get()->toArray();
        return response()->json([ 'users' => $data, 'cities' => $cities, 'properties' => $properties, 'universities' => $universities ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
             'props' => 'array|min:1',
             'props.*' => 'string|distinct|min:1',
             'faculties' => 'array|min:1',
             'faculties.*' => 'string|distinct|min:1',
             'is_active' => 'required',
             'user_id' => 'required|numeric',
             'type_id' => 'required|numeric',
             'city_id' => 'required|numeric',
             'district_id' => 'required|numeric',
             'name' => 'required',
             'slug' => 'required',
             'keywords' => 'not_in:null,undefined',
             'description' => 'max:160|not_in:null,undefined',
             'member' => 'not_in:null,undefined',
             'capacity' => 'required|numeric|not_in:null,undefined',
             'is_wifi' => 'required|not_in:null,undefined',
             'meal' => 'not_in:null,undefined',
             'content' => 'not_in:null,undefined',
             'phone' => array('regex:/(0)[0-9]{10}|(444)[0-9]{4}/', 'not_in:null,undefined'),
             'mobile_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
             'message_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
             'fax' => 'not_in:null,undefined',
             'email' => 'required|email',
             'web' => 'not_in:null,undefined',
             'address' => 'required|not_in:null,undefined',
             'lat' => 'not_in:null,undefined',
             'lng' => 'not_in:null,undefined',
             'note' => 'not_in:null,undefined',
             'center_name' => 'not_in:null,undefined',
             'center_slug' => 'not_in:null,undefined',
         ]);

        $table = new Dorm;
        $table->user_id = $request->user_id;
        $table->dorm_type_id = $request->type_id;
        $table->city_id = $request->city_id;
        $table->district_id = $request->district_id;
        $table->name = $request->name;
        $table->slug = $request->slug;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        $table->member = $request->member;
        $table->capacity = $request->capacity;
        $table->is_wifi = $request->is_wifi;
        $table->meal = $request->meal;
        $table->content = $request->input('content');
        $table->content_en = $request->content_en;
        $table->facilities = $request->facilities;
        $table->advantages = $request->advantages;
        $table->events = $request->events;
        $table->transportation = $request->transportation;
        $table->other = $request->other;
        $table->phone = $request->phone;
        $table->mobile_phone = $request->mobile_phone;
        $table->phone_for_message = $request->message_phone;
        $table->fax = $request->fax;
        $table->email = $request->email;
        $table->web = $request->web;
        $table->address = $request->address;
        $table->lat = $request->lat;
        $table->lng = $request->lng;
        $table->virtual_tour = $request->virtual_tour;
        $table->note = $request->note;
        $table->center_name = $request->center_name;
        $table->center_slug = $request->center_slug;

        if($table->save()){
            if($request->has('props')) {
                $table->properties()->attach($request->props);
            }
            if($request->has('faculties')) {
                $table->faculties()->attach($request->faculties);
            }
            if($request->hasFile('images')) {
                $this->imageUpload($request->images, $table->id);
            }
            if($request->hasFile('logo')) {
                $this->logoUpload($request->logo, $table->id);
            }
            if($request->hasFile('cover')) {
                $this->coverUpload($request->cover, $table->id);
            }
            if($request->has('video_url')) {
                $table->video()->create([
                    'url' => $request->video_url,
                    'type' => $request->video_type,
                    'is_active' => $request->video_status,
                ]);
            }
            return response()->json([ 'id' => $table->id ], 201);
        }else{
            return response()->json(['failed' => true], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Dorm::with([ 'city:id,name', 'district:id,name', 'type', 'video', 'properties:id', 'user', 'images', 'faculties' => function($q){
            $q->groupBy('id');
        }, 'faculties.university:id,name'])->findOrFail($id)->toArray();
        $users = User::select('id', 'first_name', 'last_name')->where('type_id', 1)->get();
        $data = [];
        foreach($users as $user) {
            $data[] = [ 'value' => $user->id, 'text' => $user->first_name .' '. $user->last_name ];
        }
        $cities = City::select('id', 'name')->get()->toArray();
        $universities = Faculty::select('id', 'name', 'university_id')->with('university:id,name')->groupBy('id')->get()->toArray();

        $properties = PC::with('properties')->get()->toArray();

        if($item){
            return response()->json(['dorm' => $item, 'users' => $data, 'universities' => $universities, 'cities' => $cities, 'properties' => $properties], 200);
        }else{
            return response()->status(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
             'props' => 'array|min:1',
             'props.*' => 'string|distinct|min:1',
             'faculties' => 'array|min:1',
             'faculties.*' => 'string|distinct|min:1',
             'is_active' => 'required',
             'user_id' => 'required|numeric',
             'type_id' => 'required|numeric',
             'city_id' => 'required|numeric',
             'district_id' => 'required|numeric',
             'name' => 'required',
             'slug' => 'required',
             'keywords' => 'not_in:null,undefined',
             'description' => 'max:160|not_in:null,undefined',
             'member' => 'not_in:null,undefined',
             'capacity' => 'required|numeric|not_in:null,undefined',
             'is_wifi' => 'required|not_in:null,undefined',
             'meal' => 'not_in:null,undefined',
             'content' => 'not_in:null,undefined',
             'phone' => array('regex:/(0)[0-9]{10}|(444)[0-9]{4}/', 'not_in:null,undefined'),
             'mobile_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
             'message_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
             'fax' => 'not_in:null,undefined',
             'email' => 'required|email',
             'web' => 'not_in:null,undefined',
             'address' => 'required|not_in:null,undefined',
             'lat' => 'not_in:null,undefined',
             'lng' => 'not_in:null,undefined',
             'note' => 'not_in:null,undefined',
             'center_name' => 'not_in:null,undefined',
             'center_slug' => 'not_in:null,undefined',
         ]);

        $table = Dorm::findOrFail($id);
        $table->user_id = $request->user_id;
        $table->dorm_type_id = $request->type_id;
        $table->city_id = $request->city_id;
        $table->district_id = $request->district_id;
        $table->name = $request->name;
        $table->slug = $request->slug;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        $table->member = $request->member;
        $table->capacity = $request->capacity;
        $table->is_wifi = $request->is_wifi;
        $table->meal = $request->meal;
        $table->content = request('content');
        $table->content_en = $request->content_en;
        $table->facilities = $request->facilities;
        $table->advantages = $request->advantages;
        $table->events = $request->events;
        $table->transportation = $request->transportation;
        $table->other = $request->other;
        $table->phone = $request->phone;
        $table->mobile_phone = $request->mobile_phone;
        $table->phone_for_message = $request->message_phone;
        $table->fax = $request->fax;
        $table->email = $request->email;
        $table->web = $request->web;
        $table->address = $request->address;
        $table->lat = $request->lat;
        $table->lng = $request->lng;
        $table->virtual_tour = $request->virtual_tour;
        $table->note = $request->note;
        $table->is_active = $request->is_active;
        $table->center_name = $request->center_name;
        $table->center_slug = $request->center_slug;

        if($table->save()){
            if($request->filled('video_url')) {
                $table->video()->updateOrCreate(
                    [ 'dorm_id' => $table->id ],
                    [
                        'url' => $request->video_url,
                        'type' => $request->video_type,
                        'is_active' => $request->video_status,
                    ]
                );
            }
            if($request->hasFile('logo')) {
                $this->logoUpload($request->logo, $id, $table->slug);
            }
            if($request->hasFile('cover')) {
                $this->coverUpload($request->cover, $id, $table->slug);
            }
            if($request->hasFile('images')) {
                $this->imageUpload($request->images, $id, $table->slug);
            }
            if($request->has('props')) {
                $table->properties()->sync($request->props);
            }
            if($request->has('faculties')) {
                $table->faculties()->sync($request->faculties);
            }
            return response()->json([ 'id' => $table->id ], 200);
        }else{
            return response()->json(['failed' => true], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dorm::findOrFail($id)->delete();
        Log::channel('slack')->critical("Deleting Dorm Id: $id by id: ". auth()->user()->id . " - email: " . auth()->user()->email );
//        $dorm = Dorm::findOrFail($id);
//        $dorm->is_active = 0;
//        $dorm->save();
        return response()->json([], 200);
    }

    public function logoUpload($image, $id, $slug)
    {
        $fileName =  $slug.'-'.time().rand().'-logo.'.$image->extension();
        Storage::disk('spaces')->putFileAs('uploads/dorms/'.$id.'/', $image, $fileName, 'public');
        $table = Dorm::findOrFail($id);
        $table->logo = $fileName;
        $table->save();
    }

    public function coverUpload($image, $id, $slug)
    {
        $fileName = $slug.'-'.time().rand().'-cover.'.$image->extension();
        Storage::disk('spaces')->putFileAs('uploads/dorms/'.$id.'/', $image, $fileName, 'public');
        $table = Dorm::findOrFail($id);
        $table->cover = $fileName;
        $table->save();
    }

    public function imageUpload($images, $id, $slug)
    {
        foreach($images as $key => $image):
            $extension = $image->extension();
            $fileName = $slug.'-'.uniqid().'.'.$extension;
            $path = 'uploads/dorms/'.$id.'/'.$fileName;
            $image = Image::make($image);
            if($image->height() > 800) {
                $image->resize(800, null, function($constraint) {
                    $constraint->aspectRatio();
                });
            }
            Storage::disk('spaces')->put($path, (string) $image->encode(), 'public');

            $table = new Gallery;
            $table->dorm_id = $id;
            $table->filename = $fileName;
            $table->save();
        endforeach;
    }

    public function imageDestroy(Request $request, $id)
    {
        if($id) {
            $item = Gallery::findOrFail($id);
            try {
                Storage::disk('spaces')->delete('/uploads/dorms/'.$item->dorm->id.'/'.$item->getImage());
                $item->delete();
                return response()->json([], 200);
            }catch(\Exception $e) {
                return response()->json($e, $e->getStatusCode());
            }
        }
    }

    public function coverDestroy($id)
    {
        $table = Dorm::findOrFail($id);
        Storage::disk('spaces')->delete('uploads/dorms/'.$id.'/'.$table->getCover());
        $table->cover = null;
        $table->save();
        return response()->json([], 200);
    }

    public function logoDestroy($id)
    {
        $table = Dorm::findOrFail($id);
        Storage::disk('spaces')->delete('uploads/dorms/'.$id.'/'.$table->getLogo());
        $table->logo = null;
        $table->save();
        return response()->json([], 200);
    }

    private function addVideo($id, $video_type, $video_url)
    {
        $table = new DormVideo;
        $table->dorm_id = $id;
        $table->video_type = $video_type;
        $table->video_url = $video_url;
        $table->save();
    }
}
