<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Faculty;
use YurtlarBurada\Models\University;

class FacultiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $faculties = Faculty::with('university')->orderBy('name', 'ASC')->paginate(20);
        $universities = University::all();

        if($request->filled('name')){
            $faculties = Faculty::where('name', 'like', '%'.$request->name.'%')->with('university')->paginate(20);
        }
        if($request->filled('university'))
        {
            $faculties = Faculty::where('university_id', $request->university)->with('university')->paginate(20);
        }

        $response = [
            'pagination' => [
                'total' => $faculties->total(),
                'per_page' => $faculties->perPage(),
                'current_page' => $faculties->currentPage(),
                'last_page' => $faculties->lastPage(),
                'from' => $faculties->firstItem(),
                'to' => $faculties->lastItem()
            ],
            'data' => $faculties,
            'universities' => $universities
        ];
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $universities = University::all();
        return response()->json($universities, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new Faculty;
        $table->name = $request->name;
        $table->university_id = $request->university;
        if($table->save()) {
            return response()->json($table, 201);
        }else {
            return response()->json([ 'failed' => true ], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faculty = Faculty::with('university')->findOrFail($id);
        $universities = University::all();
        $data = [ 'faculty' => $faculty, 'universities' => $universities ];
        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Faculty::findOrFail($id);
        $table->university_id = $request->university;
        $table->name = $request->name;
        $table->title = $request->title;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        $table->spot = $request->spot;
        $table->list_seo = $request->list_seo;
        $table->male_title = $request->male_title;
        $table->male_keywords = $request->male_keywords;
        $table->male_description = $request->male_description;
        $table->male_spot = $request->male_spot;
        $table->female_title = $request->female_title;
        $table->female_keywords = $request->female_keywords;
        $table->female_description = $request->female_description;
        $table->female_spot = $request->female_spot;
        $table->save();
        return response()->json([ 'success' => true ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faculty::findOrFail($id)->delete();
        return response()->json([], 200);
    }

    public function getFaculties($university_id)
    {
        $item = Faculty::where('university_id', $university_id)->select('id', 'name')->get();
        return response()->json($item, 200);
    }
}
