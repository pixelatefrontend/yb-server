<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\District;

class LocationsController extends Controller
{
    public function getCities()
    {
        $cities = City::all();
        return response()->json($cities, 200);
    }

    public function getDistricts($city_id)
    {
        $districts = District::where('city_id', $city_id)->get();
        return response()->json($districts, 200);
    }
}
