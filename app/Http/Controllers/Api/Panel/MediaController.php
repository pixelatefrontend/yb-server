<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App;
use YurtlarBurada\File;

class MediaController extends Controller
{
	private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
	private $audio_ext = ['mp3', 'ogg', 'mpga'];
	private $video_ext = ['mp4', 'mpeg'];
	private $document_ext = ['doc', 'docx', 'pdf', 'odt'];
	
	public function index(Request $request)
	{

	}
}
