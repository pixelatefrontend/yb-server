<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Promote;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\District;
use YurtlarBurada\Models\University;
use YurtlarBurada\Models\Faculty;
use YurtlarBurada\Models\DormPromotePivot as DP;

class PromotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotes = DP::with(['promote', 'university:id,name', 'dorm:id,name', 'city:id,name', 'district:id,name'])->get();
        $dorm_types = Promote::all();
        if(request()->filled('promote_type')) {
            $promotes = DP::where('promote_id', request('promote_type'))->with(['promote', 'dorm:id,name', 'city:id,name', 'district:id,name'])->get();
        }
        $data = [];
        foreach($promotes as $promote) {
            if($promote->dorm && $promote->promote) {
                $data[] = [ 'id' => $promote->id,
                    'start_date' => $promote->start_date,
                    'end_date' => $promote->end_date,
                    'dorm' => $promote->dorm,
                    'city' => $promote->city,
                    'district' => $promote->district,
                    'promote' => $promote->promote->name,
                    'university' => $promote->university ];
            }
        }
        return response()->json([ 'data' => $data, 'promotes' => $dorm_types ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dorms = Dorm::select('id', 'name')->where('is_active', 1)->get();
        $data = [];
        foreach($dorms as $dorm) {
            $data[] = [ 'value' => $dorm->id, 'text' => $dorm->name ];
        }

        return response()->json([
            'dorms' => $data,
            'promote_types' => Promote::all(),
            'cities' => City::select('id', 'name')->get(),
            'universities' => University::select('id', 'name')->get()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new DP;
        $table->dorm_id = $request->dorm;
        $table->promote_id = $request->promote_type;
        $table->city_id = $request->city;
        $table->district_id = $request->district;
        $table->university_id = $request->university;
        $table->faculty_id = $request->faculty;
        $table->dorm_type_id = $request->dorm_type;
        $table->sort = $request->sort;
        $table->color = $request->color;
        $table->top_title = $request->top_title;
        $table->bottom_title = $request->bottom_title;
        $table->start_date = $request->start_date;
        $table->end_date = $request->end_date;
        if($table->save()) {
            return response()->json($table, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promote = DP::with('dorm', 'promote', 'city', 'district', 'university', 'faculty', 'dormType')->findOrFail($id);
        $promote['start_date'] = date('Y-m-d', strtotime($promote->start_date));
        $promote['end_date'] = date('Y-m-d', strtotime($promote->end_date));
        $dorms = Dorm::where('is_active', 1)->select('id', 'name')->get();
        $dormsData = [];
        $promote_types = Promote::all();
        $cities = City::select('id', 'name')->get();
        $districts = District::where('city_id', $promote->city_id)->get();
        $universities = University::select('id', 'name')->get();
        $faculties = Faculty::where('university_id', $promote->university_id)->select('id', 'name')->get();

        foreach($dorms as $dorm) {
            $dormsData[] = [ 'value' => $dorm->id, 'text' => $dorm->name ];
        }
        $data = [  
                'promote' => $promote, 
                'promote_types' => $promote_types, 
                'dorms' => $dormsData, 
                'cities' => $cities, 
                'districts' => $districts,
                'universities' => $universities,
                'faculties' => $faculties, 
                ];
        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = DP::findOrFail($id);
        $table->dorm_id = $request->dorm;
        $table->promote_id = $request->promote_type;
        $table->city_id = $request->city;
        $table->district_id = $request->district;
        $table->university_id = $request->university;
        $table->faculty_id = $request->faculty;
        $table->dorm_type_id = $request->dorm_type;
        $table->sort = $request->sort;
        $table->color = $request->color;
        $table->top_title = $request->top_title;
        $table->bottom_title = $request->bottom_title;
        $table->start_date = $request->start_date;
        $table->end_date = $request->end_date;
        if($table->save()) {
            return response()->json([], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DP::findOrFail($id)->delete();
        return response()->json([], 200);
    }
}
