<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\SiteSetting as SS;
use Intervention\Image\ImageManagerStatic as Image;


class SiteSettingsController extends Controller
{
    public function getData()
    {
        $data = SS::where('id', 1)->firstOrFail();
        return response()->json($data, 200);
    }

    public function postData(Request $request, $id)
    {
        $table = SS::findOrFail($id);
        $table->title = $request->title;
        $table->description = $request->description;
        $table->keywords = $request->keywords;
        $table->about = $request->about;
        $table->member = $request->member;
        $table->address = $request->address;
        $table->land_phone = $request->land_phone;
        $table->mobile_phone = $request->mobile_phone;
        $table->fax = $request->fax;
        $table->email = $request->email;
        $table->home_title = $request->home_title;
        $table->home_slogan = $request->home_slogan;
        $table->home_spot = $request->home_spot;
        $table->blog_section_title = $request->blog_section_title;
        $table->blog_section_description = $request->blog_section_description;
        $table->city_section_spot = $request->city_section_spot;
        $table->copyright = $request->copyright;
        $table->facebook = $request->facebook;
        $table->twitter = $request->twitter;
        $table->instagram = $request->instagram;
        $table->linkedin = $request->linkedin;
        if($request->hasFile('logo')) {
            $this->logoUpload($request->logo, $id);
        }
        if($table->save()) {
            return response()->json([], 200);
        }
    }

    public function logoUpload($image, $id)
    {
        $fileName = time().rand().'-logo.'.$image->extension();
        $path = Storage::disk('spaces')->putFileAs('uploads/settings/', $image, $fileName, 'public');
        $table = SS::findOrFail($id);
        $table->image = $fileName;
        $table->save();
    }
}
