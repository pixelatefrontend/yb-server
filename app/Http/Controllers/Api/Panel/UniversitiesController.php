<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\University;
use YurtlarBurada\Models\UniversityImage as Gallery;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UniversitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('name')) {
            $universities = University::where('name', 'LIKE', '%'.$request->name.'%')->with('city')->get()->toArray();
        }elseif($request->has('city')){
            $universities = University::where('city_id', $request->city)->with('city')->get()->toArray();
        }else {
            $universities = University::with('city')->get()->toArray();
        }
        $cities = City::all()->toArray();
        return response()->json([ 'universities' => $universities, 'cities' => $cities ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $cities = City::all();
        return response()->json($cities, 200);
    }

    public function store(Request $request)
    {
        $table = new University;
        $table->city_id = $request->city;
        $table->name = $request->name;
        $table->slug = Str::slug($request->name);
        $table->lat = $request->lat;
        $table->lng = $request->lng;
        $table->content = $request->content;
        $table->website = $request->website;
        $table->address = $request->address;
        if($table->save()) {
            if($request->hasFile('logo')) {
                $this->logoUpload($request->logo, $table->id);
            }
            return response()->json([ 'id' => $table->id ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $university = University::with('city', 'images')->findOrFail($id);
        $cities = City::all();
        return response()->json([ 'university' => $university, 'cities' => $cities ], 200);
    }

    public function edit($id)
    {
        $university = University::with('city', 'images')->findOrFail($id);
        $cities = City::all();
        return response()->json([ 'university' => $university, 'cities' => $cities ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = University::findOrFail($id);
        $table->city_id = $request->city;
        $table->name = $request->name;
        $table->slug = Str::slug($request->name);
        $table->title = $request->title;
        $table->keywords = $request->keywords;
        $table->description = $request->description;
        $table->spot = $request->spot;
        $table->list_seo = $request->list_seo;
        //
        $table->lat = $request->lat;
        $table->lng = $request->lng;
        $table->content = $request->content;
        $table->website = $request->website;
        $table->address = $request->address;
        // Detail
        $table->detail_title = $request->detail_title;
        $table->detail_description = $request->detail_description;
        $table->detail_spot = $request->detail_spot;
        $table->detail_keywords = $request->detail_keywords;
        // Male 
        $table->male_title = $request->male_title;
        $table->male_keywords = $request->male_keywords;
        $table->male_description = $request->male_description;
        $table->male_spot = $request->male_spot;
        // Female
        $table->female_title = $request->female_title;
        $table->female_keywords = $request->female_keywords;
        $table->female_description = $request->female_description;
        $table->female_spot = $request->female_spot;
        /* Save */
        if($table->save()) {
            if($request->hasFile('logo')) {
                $this->logoUpload($request->file('logo'), $id);
            }
            if($request->hasFile('images')) {
                $this->galleryUpload($request->images, $id);
            }
            return response()->json(['success' => true], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Univeristy::findOrFail($id)->delete();
        return response()->json([], 200);
    }

    public function logoUpload($image, $id)
    {
        $fileName = time().rand().'.'.$image->extension();
        $path = Storage::disk('spaces')->putFileAs('uploads/universities/'.$id.'/', $image, $fileName, 'public');
        $table = University::findOrFail($id);
        $table->logo = $fileName;
        $table->save();
    } 

    public function galleryUpload($images, $id)
    {
        foreach($images as $key => $image):
            $extension = $image->extension();
            $fileName = str_random() . uniqid().'.'.$extension; 
            $path = 'uploads/universities/'.$id.'/'.$fileName;
            $image = Image::make($image);
            if($image->height() > 800) {
                $image->resize(800, null, function($constraint) {
                    $constraint->aspectRatio();
                });
            }
            Storage::disk('spaces')->put($path, (string) $image->encode(), 'public');

            $table = new Gallery;
            $table->university_id = $id;
            $table->filename = $fileName;
            $table->save();
        endforeach;
    }

    public function imageDestroy(Request $request, $id)
    {
        if($id) {
            $item = Gallery::with('university')->findOrFail($id);
            try {
                Storage::disk('spaces')->delete('/uploads/cities/'.$item->university->id.'/'.$item->getImage());
                $item->delete();
                return response()->json([], 200);
            }catch(\Exception $e) {
                return response()->json($e, $e->getStatusCode());
            }
        }
    }
}
