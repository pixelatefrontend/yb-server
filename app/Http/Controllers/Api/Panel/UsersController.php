<?php

namespace YurtlarBurada\Http\Controllers\Api\Panel;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\User;
use YurtlarBurada\Models\University;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->input('name');
        $user_type = $request->input('user_type');

        $users = User::with('type')
            ->when($name, function ($query) use ($name) {
                return $query->where(function ($query) use ($name) {
                    $query->where('first_name', 'like', '%'.$name.'%')
                        ->orWhere('last_name', 'like', '%'.$name.'%')
                        ->orWhere('email', 'like', '%'.$name.'%');
                });
            })
            ->when($user_type, function ($query) use ($user_type) {
                return $query->where('type_id', $user_type);
            })
            ->orderBy('id','DESC')
            ->paginate(50);

        $response = [
            'pagination' => [
                'total' => $users->total(),
                'per_page' => $users->perPage(),
                'current_page' => $users->currentPage(),
                'last_page' => $users->lastPage(),
                'from' => $users->firstItem(),
                'to' => $users->lastItem()
            ],
            'data' => $users
        ];
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'is_active' => 'required|numeric',
            'dorm_id' => 'numeric',
            'type' => 'required|numeric',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'phone' => 'regex:/(0)[0-9]{10}/',
        ]);

        $table = new User;
        $table->is_active = $request->is_active;
        $table->type_id = $request->type;
        $table->first_name = $request->first_name;
        $table->last_name = $request->last_name;
        $table->email = $request->email;
        $table->password = bcrypt($request->password);
        $table->phone = $request->phone;
        $table->dorm_id = $request->dorm_id;
        if($table->save()) {
            return response()->json($table, 201);
        }else{
            return response()->json(['failed' => true], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('type', 'university', 'faculty')->findOrFail($id);
        return response()->json($user, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('type', 'university', 'faculty')->findOrFail($id);
        $universities = University::all();
        return response()->json(['user' => $user, 'universities' => $universities], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = array_map('trim', $request->all());
        $input['phone'] = str_replace(' ', '', $request->input('phone'));
        $request->replace($input);
        $this->validate($request, [
            'university' => 'numeric',
            'is_active' => 'required|numeric',
            'dorm_id' => 'numeric',
            'type' => 'required|numeric',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'min:6',
            'phone' => 'regex:/(0)[0-9]{10}/',
        ]);

        $table = User::findOrFail($id);
        $table->university_id = $request->university;
        $table->first_name = $request->first_name;
        $table->last_name = $request->last_name;
        $table->email = $request->email;
        $table->password = $request->password == '' ? $table->password : bcrypt($request->password);
        $table->phone = $request->phone;
        $table->is_active = $request->is_active;
        $table->dorm_id = $request->dorm_id;
        $table->type_id = $request->type;
        if($table->save()) {
            return response()->json(['success' => true], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        return response()->json([], 200);
    }
}
