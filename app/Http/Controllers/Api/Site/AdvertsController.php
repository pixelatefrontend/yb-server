<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Advert;

class AdvertsController extends Controller
{
    public function list(Request $request)
    {
        $take = $request->input('take', 9);
        $items = Advert::orderBy('id', 'ASC')->take($take)->get();
        return response()->json($items, 200);
    }

    public function detail($slug, $id)
    {
        $item = Advert::with('images')->where('slug', $slug)->where('id', $id)->first();
        return response()->json($item, 200);
    }
}
