<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Blog;
use YurtlarBurada\Models\BlogCategory;
use YurtlarBurada\Models\DormPromotePivot as DP;

class BlogsController extends Controller
{
    public function list()
    {
        $items = Blog::with('author', 'category')->orderBy('id', 'DESC')->paginate(9);
        return response()->json($items, 200);
    }

    public function detail($id)
    {
        $item = Blog::with('images', 'category', 'author')->findOrFail($id);
        $data = DP::where('promote_id', 8)->with('dorm.type','dorm.city', 'dorm.district')->orderBy('sort', 'ASC')->get();
        $dorms = [];
        foreach($data as $row):
            $dorms[] = $row->dorm;
        endforeach;
        return response()->json([ 'blog' => $item, 'dorms' => $dorms ], 200);
    }

    public function category($slug)
    {
        $items = BlogCategory::with('blogs.images', 'blogs.author')->where('slug', $slug)->firstOrFail();
        return response()->json($items, 200);
    }

    public function categories()
    {
        $items = BlogCategory::all();
        return response()->json($items, 200);
    }
}
