<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\DormPromotePivot as DP;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use YurtlarBurada\Models\District;
use YurtlarBurada\Models\Promote;

class CitiesController extends Controller
{
    public function cities()
    {
        $cities = City::withCount(['dorms' => function($q) {
            $q->active();
        }])->orderBy('id', 'ASC')->get();

        return response()->json($cities, 200);
    }

    public function popularCities(Request $request)
    {
        $take = $request->input('take', 8);
        $cities = City::take($take)->withCount(['dorms' => function($q) {
            $q->active();
        }])->orderBy('dorms_count', 'DESC')->get();

        return response()->json($cities, 200);
    }

    public function detail($slug)
    {
        $city = City::where('slug', $slug)->withCount(['dorms' => function($q) {
            $q->active();
        }])->withCount('universities')->with('universities', 'districts', 'images')->firstOrFail();

        return response()->json($city, 200);
    }

    public function dorms(Request $request, $slug)
    {
        $city = City::where('slug', $slug)->first();
        if (empty($city)) abort(404);

        $page = request()->input('page', 1);

        if($request->filled('district') && $request->filled('dorm_type')) {
            $district = District::where('slug', $request->district)->first()->id;
            $gender = $request->dorm_type;
            $districtDataWithGender = $this->districtFilterWithGender($district, $gender, $page, $request->url(), $request->query());
            return response()->json($districtDataWithGender, 200);
        }

        if($request->filled('district')) {
            $district = District::where('slug', $request->district)->first()->id;
            $districtData = $this->districtFilter($district, $page, $request->url(), $request->query());
            return response()->json($districtData, 200);
        }

        if($request->filled('dorm_type')) {
            $citiesData = $this->genderFilter($city->id, $request->dorm_type, $page, $request->url(), $request->query());
            return response()->json($citiesData, 200);
        }

        $list = $this->list($city->id, $page, $request->url(), $request->query());
        return response()->json($list, 200);
    }

    public function list($city, $page, $url, $query)
    {
        $perPage = 40;

        $city = City::with(['dorms' => function($q){
            $q->selectPublicFields()->inRandomOrder();
        }, 'dorms.city:id,name,slug', 'dorms.district:id,name,slug', 'districts'])
            ->findOrFail($city);

        $promotedDorms = $this->getPromotedDorms(['cityId' => $city->id]);

        $promote_ids = collect($promotedDorms)->pluck('id')->toArray();

        $normalDorms = $city->dorms->whereNotIn('id', $promote_ids)->shuffle()->toArray();

        $dorms = array_merge($promotedDorms, $normalDorms);

        $banners = $this->getBanners(['cityId' => $city->id]);

        $data = $this->mergeWithBanner($dorms, $banners);

        $popupDorms = $this->getPopupDorms(['cityId' => $city->id]);

        $dataWithPagination = new Paginator($data->forPage($page,$perPage), count($data), $perPage, $page, ['path' => $url, 'query' => $query]);

        return ['city' => $city->unsetRelation('dorms')->unsetRelation('districts'), 'data' => $dataWithPagination, 'popups' => $popupDorms];
    }

    public function districtFilter($district, $page, $url, $query)
    {
        $perPage = 40;

        $district = District::with(['dorms' => function($q){
            $q->selectPublicFields()->inRandomOrder();
        }, 'dorms.city:id,slug,name', 'dorms.district:id,slug,name', 'city:id,slug,name'])->where('id', $district)->first();

        $promotedDorms = $this->getPromotedDorms(['districtId' => $district->id]);

        $promote_ids = collect($promotedDorms)->pluck('id')->toArray();

        $normalDorms = $district->dorms->whereNotIn('id', $promote_ids)->shuffle()->toArray();

        $dorms = array_merge($promotedDorms, $normalDorms);

        $banners = $this->getBanners(['districtId' => $district->id]);

        $data = $this->mergeWithBanner($dorms, $banners);

        $popupDorms = $this->getPopupDorms(['districtId' => $district->id]);

        $dataWithPagination = new Paginator($data->forPage($page,$perPage), count($data), $perPage, $page, ['path' => $url, 'query' => $query]);

        return ['district' => $district->unsetRelation('dorms'), 'data' => $dataWithPagination, 'popups' => $popupDorms];

    }

    public function genderFilter($city, $gender, $page, $url, $query)
    {
        $perPage = 40;

        $city = City::with(['dorms' => function($q) use($gender){
            $q->selectPublicFields()->where('dorm_type_id', $gender)->inRandomOrder();
        }, 'dorms.city:id,slug,name', 'dorms.district:id,slug,name', 'districts'])->findOrFail($city);

        $promotedDorms = $this->getPromotedDorms(['cityId' => $city->id, 'dormTypeId' => $gender]);

        $promote_ids = collect($promotedDorms)->pluck('id')->toArray();

        $normalDorms = $city->dorms->whereNotIn('id', $promote_ids)->shuffle()->toArray();

        $dorms = array_merge($promotedDorms, $normalDorms);

        $banners = $this->getBanners(['cityId' => $city->id, 'dormTypeId' => $gender]);

        $data = $this->mergeWithBanner($dorms, $banners);

        $popupDorms = $this->getPopupDorms(['cityId' => $city->id, 'dormTypeId' => $gender]);

        $dataWithPagination  =  new Paginator($data->forPage($page,$perPage), count($data), $perPage, $page, ['path' => $url, 'query' => $query]);

        return ['city' => $city->unsetRelation('dorms')->unsetRelation('districts'), 'data' => $dataWithPagination, 'popups' => $popupDorms];
    }

    public function districtFilterWithGender($district, $gender, $page, $url, $query)
    {
        $perPage = 40;

        $district = District::with(['dorms' => function($q) use($gender){
            $q->selectPublicFields()->where('dorm_type_id', $gender)->inRandomOrder();
        }, 'dorms.city:id,slug,name', 'dorms.district:id,slug,name', 'city:id,slug,name'])->where('id', $district)->first();

        $promotedDorms = $this->getPromotedDorms(['districtId' => $district->id, 'dormTypeId' => $gender]);

        $promote_ids = collect($promotedDorms)->pluck('id')->toArray();

        $normalDorms = $district->dorms->whereNotIn('id', $promote_ids)->shuffle()->toArray();

        $dorms = array_merge($promotedDorms, $normalDorms);

        $banners = $this->getBanners(['districtId' => $district->id, 'dormTypeId' => $gender]);

        $data = $this->mergeWithBanner($dorms, $banners);

        $popupDorms = $this->getPopupDorms(['districtId' => $district->id, 'dormTypeId' => $gender]);

        $dataWithPagination  =  new Paginator($data->forPage($page,$perPage), count($data), $perPage, $page, ['path' => $url, 'query' => $query]);

        return ['district' => $district->unsetRelation('dorms'), 'data' => $dataWithPagination, 'popups' => $popupDorms];
    }

    /**
     * @param array $opts
     * optional ['cityId', 'districtId', 'dormTypeId']
     *
     * @return array
     */
    public function getPromotedDorms($opts) {
        $districtId = $opts['districtId'] ?? null;
        $dormTypeId = $opts['dormTypeId'] ?? null;

        $firstDorm = DP::where('promote_id', $this->getPromoteId('doping')) // doping = 3
        ->with(['dorm' => function($q){
            $q->selectPublicFields();
        }, 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type'])
            ->where([
                [ 'university_id', '=', null ],
                [ 'faculty_id', '=', null ],
                [ 'district_id', '=', $districtId ],
                [ 'dorm_type_id', '=', $dormTypeId ]
            ]);
        if (!empty($opts['cityId'])) {
            $firstDorm = $firstDorm->where('city_id', $opts['cityId']);
        }
        $firstDorm = $firstDorm->inRandomOrder()->first();
        if($firstDorm && $firstDorm->dorm) {
            $firstDorm->dorm->append('branch_count');
            $firstDorm->dorm['is_advert'] = 1;
            $firstDorm->dorm['promote_id'] = 3;
        }

        $listDorms = DP::where('promote_id', $this->getPromoteId('listeleme-sabit')) // listeleme-sabit = 4
            ->with(['dorm' => function($q){
                $q->selectPublicFields();
            }, 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type'])
            ->where([
                [ 'university_id', '=', null ],
                [ 'faculty_id', '=', null ],
                [ 'district_id', '=', $districtId],
                [ 'dorm_type_id', '=', $dormTypeId ]
            ]);
        if (!empty($opts['cityId'])) {
            $listDorms = $listDorms->where('city_id', $opts['cityId']);
        }
        $listDorms = $listDorms->orderBy('sort', 'ASC')->get();

        $listDorms2 = DP::where('promote_id', $this->getPromoteId('listeleme')) // listeleme = 5
            ->with(['dorm' => function($q){
                $q->selectPublicFields();
            }, 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type'])
            ->where([
                [ 'university_id', '=', null ],
                [ 'faculty_id', '=', null ],
                [ 'district_id', '=', $districtId],
                [ 'dorm_type_id', '=', $dormTypeId ]
            ]);
        if (!empty($opts['cityId'])) {
            $listDorms2 = $listDorms2->where('city_id', $opts['cityId']);
        }
        $listDorms2 = $listDorms2->inRandomOrder()->get();

        $listDormsData = [];
        foreach($listDorms as $row) {
            if (!empty($row->dorm)) {
                $row->dorm['promote_id'] = 4;
//                $row->dorm->append('branch_count');
                $listDormsData[] = $row->dorm->toArray();
            }
        }

        $listDormsData2 = [];
        foreach($listDorms2 as $row) {
            if (!empty($row->dorm)) {
                $row->dorm['promote_id'] = 5;
//                $row->dorm->append('branch_count');
                $listDormsData2[] = $row->dorm->toArray();
            }
        }

        $data = array_merge($firstDorm && $firstDorm->dorm ? array($firstDorm->dorm->toArray()) : [],
            $listDormsData, $listDormsData2);
        return $data;
    }

    /**
     * @param array $opts
     * optional ['cityId', 'districtId', 'dormTypeId']
     *
     * @return array
     */
    public function getPopupDorms($opts) {
        $districtId = $opts['districtId'] ?? null;
        $dormTypeId = $opts['dormTypeId'] ?? null;
        $popupDorms = DP::where('promote_id', $this->getPromoteId('popup')) // popup = 6
            ->with('dorm:id,name,slug,cover,city_id,user_id,district_id,meal,capacity,dorm_type_id', 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type')
            ->where([
                [ 'university_id', '=', null ],
                [ 'faculty_id', '=', null ],
                [ 'district_id', '=', $districtId],
                [ 'dorm_type_id', '=', $dormTypeId ]
            ]);
        if (!empty($opts['cityId'])) {
            $popupDorms = $popupDorms->where('city_id', $opts['cityId']);
        }
        $popupDorms = $popupDorms->orderBy('sort', 'ASC')->get();
        return $popupDorms;
    }

    /**
     * @param array $opts
     * optional ['cityId', 'districtId', 'dormTypeId']
     *
     * @return array
     */
    public function getBanners($opts) {
        $cityId = $opts['cityId'] ?? null;
        $dormTypeId = $opts['dormTypeId'] ?? null;
        $districtId = $opts['districtId'] ?? null;

        $banner = DP::where('promote_id', $this->getPromoteId('banner')) // banner = 9
            ->with(['dorm' => function($q){
                $q->selectPublicFields();
            }, 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type'])
            ->where('city_id', $cityId)
            ->where([[ 'district_id', '=', $districtId ], [ 'university_id', '=', null ], [ 'faculty_id', '=', null ], [ 'dorm_type_id', '=', $dormTypeId ]])
            ->orderBy('sort', 'ASC')
            ->get()->toArray();

        return $banner;
    }

    public function mergeWithBanner($dorms, $banners) {
        $dorms = array_chunk($dorms, 8);
        $banners = array_chunk($banners, 2);
        foreach ($dorms as $key=>$item) {
            if (!empty($banners[0])) {
                array_push($dorms[$key], ['banners'=>$banners[0]]);
                array_shift($banners);
            }
        }
        $data = collect($dorms)->collapse();
        return $data;
    }

    public function getPromoteId($slug) {
        $banner = Promote::where('slug', $slug)->first();
        return $banner ? $banner->id : null;
    }


}
