<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use YurtlarBurada\Models\Contact;
use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function postMessage()
    {
        request()->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);
        $table = new Contact;
        $table->name = request('name');
        $table->phone = request('phone');
        $table->email = request('email');
        $table->message = request('message');
        if($table->save()) {
            return response()->json([], 200);
        }else {
            return response()->json([], 500);
        }
    }
}
