<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use YurtlarBurada\Models\City;
use YurtlarBurada\Models\District;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\DormRoom;
use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;

class DormRoomsController extends Controller
{

    /**
     * @param $dorm_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoomsOfDorm($dorm_id) {
        $rooms = DormRoom::where('dorm_id', $dorm_id)->saleActive()->get();
        return response()->json($rooms, 200);
    }

    /**
     * @param \Illuminate\Http\Request $req
     * @param $slug
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFilteredRooms(Request $req, $slug) {
        $limit = $req->limit ?? 12;
        $city = City::where('slug', $slug)->first();
        if (empty($city)) return response()->json(null, 404);
        $rooms = Dorm::where('city_id', $city->id)
            ->with([
                'city:id,name,slug',
                'district:id,slug,name',
                'rooms' => function($q) use($req) {
                    if ($req->capacity) {
                        $q->where('capacity', $req->capacity);
                    }
                    if ($req->minDailyPrice) {
                        $q->where('daily_price', '>=', $req->minDailyPrice);
                    }
                    if ($req->maxDailyPrice) {
                        $q->where('daily_price', '<=', $req->maxDailyPrice);
                    }
                    $q->saleActive();
                }])
            ->withCount([
                'rooms' => function($q) {
                    $q->saleActive();
                }])->orderBy('rooms_count','DESC');

        if ($req->district) {
            $rooms = $rooms->where('district_id', $req->district);
        }

        $response = $rooms
            ->has('rooms', '>', 0)
            ->paginate($limit);
        return response()->json($response, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCitiesOfRooms() {
        $city = City::select(['id','slug','name','image'])->withCount(['rooms'])->orderBy('rooms_count','DESC');
        $response = [
            'promoted' => $city->take(7)->get(),
            'others' => $city->skip(7)->take(80)->get()
        ];
        return response()->json($response, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDistrictsOfRooms() {
        $city = District::select(['id','slug','name'])->withCount(['rooms'])->orderBy('rooms_count','DESC');
        return response()->json($city->get(), 200);
    }
}
