<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use Illuminate\Http\Request;
use YurtlarBurada\Helpers\SmsApi;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\District;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\DormPhoneClick;
use YurtlarBurada\Models\DormProperty as Property;
use YurtlarBurada\Models\DormComment;
use YurtlarBurada\Models\DormMessage;
use YurtlarBurada\Models\DormReservation;
use YurtlarBurada\Models\DormPhoneClick as Click;
use YurtlarBurada\Models\Faculty;
use YurtlarBurada\Models\PropertyCategory as Category;
use YurtlarBurada\Models\University;

class DormsController extends Controller
{
    public function list()
    {
        $items = Dorm::active()->get();
        return response()->json($items);
    }

    public function detailByOldId($old_id)
    {
        $item = Dorm::with('images', 'comments.replies', 'properties', 'comments.user', 'city:id,slug,name', 'district:id,slug,name')->active()->where('old_id', $old_id)->first();
        if ($item) {
            $properties = Category::with(['properties' => function ($q) use ($item) {
                \DB::table('dorm_property_pivot')->where('dorm_id', $item->id);
            }])->get();

            $branches = Dorm::where('user_id', $item->user_id)->with('city:id,slug,name', 'district:id,slug,name')->active()->get();

            $similarDorms = Dorm::where('id', '!=', $item->id)
                ->where('city_id', $item->city_id)
                ->active()
                ->with('city:id,slug,name', 'district:id,slug,name')
                ->take('12')
                ->get();
            $item->addView();
            $item->makeHidden(['note']);
            $data = ['dorm' => $item, 'properties' => $properties, 'similarDorms' => $similarDorms, 'branches' => $branches];
            return response()->json($data, 200);
        } else {
            return response()->json([], 404);
        }
    }

    public function detail($id)
    {
        $item = Dorm::with(['images', 'comments.replies', 'properties', 'comments.user', 'city:id,slug,name', 'district:id,slug,name', 'video' => function($q){
            $q->where('is_active', 1);
        }])->active()->findOrFail($id);
        if ($item) {
            $properties = Category::with(['properties' => function ($q) use ($item) {
                \DB::table('dorm_property_pivot')->where('dorm_id', $item->id);
            }])->get();

            $branches = Dorm::where('user_id', $item->user_id)->with('city:id,slug,name', 'district:id,slug,name')->active()->get();

            $similarDorms = Dorm::where('id', '!=', $item->id)
                ->where('city_id', $item->city_id)
                ->active()
                ->with('city:id,slug,name', 'district:id,slug,name')
                ->take('12')
                ->get();
            $item->addView();
            $item->makeHidden(['note']);
            $data = ['dorm' => $item, 'properties' => $properties, 'similarDorms' => $similarDorms, 'branches' => $branches];
            return response()->json($data, 200);
        } else {
            return response()->json([], 404);
        }
    }

    public function postComment(Request $request, $id)
    {
        $table = new DormComment;
        $table->user_id = auth()->user()->id;
        $table->dorm_id = $id;
        $table->comment = $request->comment;
        if ($table->save()) {
            return response()->json([], 200);
        } else {
            return response()->json([], 500);
        }
    }

    public function viewNumberButton(Request $request, $id)
    {
        $table = new DormPhoneClick;
        $table->dorm_id = $id;
        $table->type = $request->type;
        $table->ip = request()->ip();
        if ($table->save()) {
            return response()->json([], 200);
        } else {
            return response()->json([], 500);
        }
    }

    public function postReservation(Request $request, $id)
    {
        $table = new DormReservation;
        $table->dorm_id = $id;
        $table->room_type_id = $request->room_type_id;
        $table->name = $request->name;
        $table->phone = $request->phone;
        $table->ip = $request->ip();
        if ($table->save()) {
            return response()->json([], 200);
        } else {
            return response()->json([], 500);
        }
    }

    public function postMessage(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|regex:/(0)[0-9]{10}/',
            'email' => 'required|email',
            'faculty_id' => 'required',
            'university_id' => 'required',
        ]);

        $dorm = Dorm::find($id);
        $university = University::find($request->university_id);
        $faculty = Faculty::find($request->faculty_id);

        if (empty($dorm) || empty($university) || empty($faculty)) response()->json([], 500);

        $toSender = str_start($request->phone, '9');
        $toDorm = preg_replace('/\D+/', '', str_start($dorm->phone_for_message, '9'));

        $message = "{$university->name} {$faculty->name}nden {$request->name} fiyat öğrenmek istiyor. Tel:{$toSender}";
        $smsDorms = SmsApi::sendSMS($message, $toDorm);
        $smsSender = SmsApi::sendSMS("YurtlarBurada.com dan ilettiğiniz mesajınız yurt sahibine / sahiplerine iletilmiştir. Teşekkür ederiz.", $toSender);

        $table = new DormMessage;
        $table->dorm_id = $id;
        $table->name = $request->name;
        $table->message = $message;
        $table->phone = $request->phone;
        $table->email = $request->email;
        $table->faculty_id = $request->faculty_id;
        $table->university_id = $request->university_id;
        $table->ip = $request->ip();
        $table->from = 'dorm';
        $table->from_id = $id;
        if ($table->save()) {
            return response()->json([$smsDorms, $smsSender, $table], 200);
        } else {
            return response()->json([], 500);
        }
    }

    public function postMultipleMessage(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|regex:/(0)[0-9]{10}/',
            'email' => 'required|email',
            'faculty_id' => 'required|numeric',
            'university_id' => 'required|numeric',
            'list' => 'required|not_in:null,undefined', //city, district, university, faculty
            'list_id' => 'required|numeric'
        ]);
        try {
            switch ($request->list) {
                case 'city':
                    $list = City::find($request->list_id);
                    $dorms = $list->dorms()->active()->pluck('phone_for_message', 'id')->toArray();
                    break;
                case 'district':
                    $list = District::find($request->list_id);
                    $dorms = $list->dorms()->active()->pluck('phone_for_message', 'id')->toArray();
                    break;
                case 'university':
                    $university = University::find($request->list_id);
                    $facultyIds = $university->faculties->pluck('id');
                    $faculties = Faculty::with(['dorms' => function ($q, $request) {
                        $q->where('dorm_type_id', $request->gender)->active();
                    }, 'dorms.city:id,name,slug', 'dorms.district:id,name,slug'])->whereIn('id', $facultyIds)->get();
                    $list = $faculties->pluck('dorms')->collapse();
                    $dorms = $list->pluck('phone_for_message', 'id')->toArray();
                    break;
                case 'faculty':
                    $list = Faculty::find($request->list_id);
                    $dorms = $list->dorms()->where('dorm_type_id', $request->gender)->active()->pluck('phone_for_message', 'id')->toArray();
                    break;
                default:
                    $dorms = [];
                    abort(500, 'List not defined!');
            }
        } catch (\Exception $exception) {
            abort(500, $exception->getMessage());
        }

        $userUniversity = University::find($request->university_id);
        $userFaculty = Faculty::find($request->faculty_id);

        if (empty($dorms) || empty($userUniversity) || empty($userFaculty)) response()->json(['error' => 'Fields empty!'], 500);

        $toSender = str_start($request->phone, '9');
        $toDorms = [];
        foreach ($dorms as $dormId => $dormPhone) {
            if (!empty($dormPhone) && $dormPhone != 'null') {
                $phone = str_start($dormPhone, '9');
                $toDorms[] = preg_replace('/\D+/', '', $phone);
            }
        }

        $message = "{$userUniversity->name} {$userFaculty->name}nden {$request->name} fiyat öğrenmek istiyor. Tel: +{$toSender}";
        $smsDorms = SmsApi::sendSMS($message, $toDorms);
        $smsSender = SmsApi::sendSMS("YurtlarBurada.com dan ilettiğiniz mesajınız yurt sahibine / sahiplerine iletilmiştir. Teşekkür ederiz.", $toSender);

        foreach ($dorms as $dormId => $dormPhone) {
            if (!empty($dormPhone) && $dormPhone != 'null') {
//                $log = " --NOTE-- From: {$request->list} Id: {$request->list_id} --";
                $table = new DormMessage;
                $table->dorm_id = $dormId;
                $table->name = $request->name;
                $table->message = $message;
                $table->phone = $request->phone;
                $table->email = $request->email;
                $table->faculty_id = $request->faculty_id;
                $table->university_id = $request->university_id;
                $table->ip = $request->ip();
                $table->from = $request->list;
                $table->from_id = $request->list_id;
                $table->save();
            }
        }
        return response()->json([$smsDorms, $smsSender], 200);
    }

    public function addFavorite(Request $request, $id)
    {
        auth()->user()->favorites()->attach($id);
        return response()->json([], 200);
    }

    public function destroyFavorite(Request $request, $id)
    {
        auth()->user()->favorites()->detach($id);
        return response()->json([], 200);
    }

    public function postPreRegister(Request $request)
    {
        $this->validate($request, [
            'member' => 'required|not_in:null,undefined',
            'name' => 'required',
            'capacity' => 'required|numeric|not_in:null,undefined',
            'email' => 'required|email',
            'phone' => 'regex:/(0)[0-9]{10}/|not_in:null,undefined',
            'mobile_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
//            'message_phone' => 'required|regex:/(0)[0-9]{10}/|not_in:null,undefined',
            'address' => 'required|not_in:null,undefined',
            'city_id' => 'numeric',
            'district_id' => 'numeric',
        ]);

        $table = new Dorm;
        $table->is_active = 0;
//        $table->city_id = $request->city_id;
//        $table->district_id = $request->district_id;
        $table->name = $request->name;
        $table->slug = str_slug($request->name);
        $table->member = $request->member;
        $table->capacity = $request->capacity;
        $table->phone = $request->phone;
        $table->mobile_phone = $request->mobile_phone;
//        $table->phone_for_message = $request->message_phone;
        $table->email = $request->email;
        $table->address = $request->address;
        if ($table->save()) {
            return response()->json(['id' => $table->id], 201);
        } else {
            return response()->json(['failed' => true], 403);
        }
    }

    public function getBranches($dorm_id)
    {
        $dorm = Dorm::active()->with([
            'branches' => function ($q) use ($dorm_id) {
                $q->active()
                    ->select('id', 'name', 'slug', 'center_name', 'center_slug', 'cover', 'city_id', 'user_id', 'district_id', 'meal', 'capacity', 'dorm_type_id', 'is_wifi')
                    ->where('user_id', '!=', '0');
            }, 'branches.city:id,slug,name', 'branches.district:id,slug,name', 'branches.type'])->with('city:id,slug,name', 'district:id,slug,name', 'type')->findOrFail($dorm_id);
        if (empty($dorm->branches->toArray())) {
            abort(404);
        }
        return response()->json($dorm);
    }
}
