<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\DormType;
use YurtlarBurada\Models\Promote;
use YurtlarBurada\Models\University;
use YurtlarBurada\Models\DormPromotePivot as DP;
use YurtlarBurada\Models\Blog;

class HomeController extends Controller
{
	public function sliderPromoteDorms(Request $request)
	{

		$take = $request->input('take', 15);
		$data = DP::where('promote_id', 1)->with(['dorm' => function($q) {
			$q->select('id', 'logo', 'name', 'slug', 'center_name', 'center_slug', 'cover', 'city_id', 'user_id', 'district_id', 'meal', 'capacity', 'dorm_type_id', 'is_wifi')->active();
		}])->orderBy('sort', 'ASC')->take($take)->get();
		$dorms = [];
		foreach($data as $row) {
            $row->dorm->append('branch_count');
			$dorms[] = $row->dorm;
		}
		return response()->json(array_filter($dorms), 200);
	}

	public function showCasePromoteDorms(Request $request)
	{
		$take  = $request->input('take', 8);
		$cities = City::select('id', 'slug', 'name')->take(4)->withCount(['dorms' => function($q) {
            $q->active();
        }])->orderBy('dorms_count', 'DESC')->get();
		if($request->filled('city')) {
			$data = DP::where('promote_id', 2)->where('city_id', $request->city)->with(['dorm' => function($q) {
				$q->select('id', 'name', 'slug', 'center_name', 'center_slug', 'cover', 'city_id', 'user_id', 'district_id', 'meal', 'capacity', 'dorm_type_id', 'is_wifi')->active();
			},'dorm.city:id,name', 'dorm.district:id,name', 'dorm.type'])->orderBy('sort', 'ASC')->take($take)->get();
		}else {
			$data = DP::where('promote_id', 2)->with(['dorm' => function($q) {
				$q->select('id', 'name', 'slug', 'center_name', 'center_slug', 'cover', 'city_id', 'user_id', 'district_id', 'meal', 'capacity', 'dorm_type_id', 'is_wifi')->active();
			},'dorm.city:id,name', 'dorm.district:id,name', 'dorm.type'])->orderBy('sort', 'ASC')->take($take)->get();
		}
		$dorms = [];
		foreach($data as $row) {
            $row->dorm->append('branch_count');
			$dorms[] = $row->dorm;
		}

		$dorms = array_filter($dorms);

		$data = [ 'cities' => $cities, 'dorms' => $dorms ];

		return response()->json($data, 200);
	}

	public function selectCities()
	{
		$cities = City::select('id', 'name', 'slug')->orderBy('name', 'ASC')->get();
		return response()->json($cities, 200);
	}

	public function universities($city_id)
	{
		$universities = University::select('id', 'name', 'slug')->where('city_id', $city_id)->orderBy('name', 'ASC')->get();
		return response()->json($universities, 200);
	}

	public function cities(Request $request)
	{
		$take = $request->input('take', 9);
		$cities = City::take($take)->withCount(['dorms' => function($q) {
            $q->active();
        }])->orderBy('dorms_count', 'DESC')->get();

		return response()->json($cities, 200);
	}

	public function blogs()
	{
		$take = request('take', 3);
		$blogs = Blog::where('is_constant', 1)->with('author')->get();
		return response()->json($blogs, 200);
	}

	public function search(Request $request)
	{

	}
}
