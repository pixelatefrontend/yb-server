<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Dorm;

class SearchController extends Controller
{
    public function getList()
    {
        $items = Dorm::select('id', 'name', 'slug')->active()->orderBy('name', 'ASC')->get();
        return response()->json($items);
    }
}
