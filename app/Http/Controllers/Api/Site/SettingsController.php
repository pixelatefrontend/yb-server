<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\SiteSetting as Setting;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\User;
use YurtlarBurada\Models\University;

class SettingsController extends Controller
{
    public function getSettings()
    {
        $item = Setting::where('id', 1)->first();
        $dormCount = Dorm::active()->count();
        $cityCount = City::all()->count();
        $userCount = User::where('type_id', 2)->count();
        $universityCount = University::all()->count();
        $data = [
            'setting' => $item,
            'dorms' => $dormCount,
            'cities' => $cityCount,
            'users' => $userCount,
            'university' => $universityCount
        ];
        return response()->json($data, 200);
    }

    public function getCounter()
    {
        $dormCount = Dorm::active()->count();
        $cityCount = City::all()->count();
        $userCount = User::where('type_id', 2)->count();
        $universityCount = University::all()->count();
        $data = [
            'dorms' => $dormCount,
            'cities' => $cityCount,
            'users' => $userCount,
            'university' => $universityCount
        ];
        return response()->json($data, 200);
    }
}
