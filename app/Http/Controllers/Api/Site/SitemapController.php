<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\University;
use YurtlarBurada\Models\District;
use YurtlarBurada\Models\Faculty;
use YurtlarBurada\Models\Blog;
use Carbon\Carbon;

class SitemapController extends Controller
{
    public function index()
    {
        $now = Carbon::now()->toAtomString();
        $content = view('sitemap', compact('now'));
        return response($content)->header('Content-Type', 'application/xml');
    }

    public function cities()
    {
        $cities = City::select('id', 'name', 'slug')->get();
        $now = Carbon::now()->toAtomString();
        $content = view('sitemap_cities', compact('cities','now'));
        return response($content)->header('Content-Type', 'application/xml');
    }

    public function districts()
    {
        $districts = District::select('id', 'name', 'slug', 'city_id')->with('city:id,slug')->get();
        $now = Carbon::now()->toAtomString();
        $content = view('sitemap_districts', compact('districts','now'));
        return response($content)->header('Content-Type', 'application/xml');
    }

    public function universities()
    {
        $universities = University::select('id', 'name', 'slug')->get();
        $now = Carbon::now()->toAtomString();
        $content = view('sitemap_universities', compact('universities','now'));
        return response($content)->header('Content-Type', 'application/xml');
    }

    public function faculties()
    {
        $faculties = Faculty::select('id', 'name', 'slug', 'university_id')->with('university:id,slug')->get();
        $now = Carbon::now()->toAtomString();
        $content = view('sitemap_faculties', compact('faculties','now'));
        return response($content)->header('Content-Type', 'application/xml');
    }

    public function dorms()
    {
        $dorms = Dorm::select('id', 'name', 'slug', 'cover')->active()->orderBy('updated_at', 'DESC')->get();
        $now = Carbon::now()->toAtomString();
        $content = view('sitemap_dorms', compact('dorms','now'));
        return response($content)->header('Content-Type', 'application/xml');
    }

    public function blogs()
    {
        $blogs = Blog::select('id', 'title', 'slug', 'cover')->get();
        $now = Carbon::now()->toAtomString();
        $content = view('sitemap_blogs', compact('blogs','now'));
        return response($content)->header('Content-Type', 'application/xml');
    }

    public function static()
    {
        $now = Carbon::now()->toAtomString();
        $content = view('sitemap_static', compact('now'));
        return response($content)->header('Content-Type', 'application/xml');
    }
}
