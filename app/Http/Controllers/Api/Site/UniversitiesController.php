<?php

namespace YurtlarBurada\Http\Controllers\Api\Site;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\University;
use YurtlarBurada\Models\Dorm;
use YurtlarBurada\Models\Faculty;
use YurtlarBurada\Models\City;
use YurtlarBurada\Models\Promote;
use YurtlarBurada\Models\DormPromotePivot as DP;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class UniversitiesController extends Controller
{
    public function universities()
    {
        $universities = University::select('id', 'name', 'logo', 'slug')->get();
        return response()->json($universities, 200);
    }

    public function filterUniversity($id)
    {
        $university = University::where('university_id', $id)->get(); // TODO burasi calismiyor gerekli mi ?
        return response()->json($university, 200);
    }

    public function detail($slug)
    {
        $university = University::where('slug', $slug)->with('faculties', 'images')->firstOrFail();
        return response()->json($university, 200);
    }

    public function dorms(Request $request, $slug)
    {
        $university = University::with(['dorms' => function($q){
            $q->where('is_active', 1);
        }, 'dorms.city:id,name,slug', 'dorms.district:id,name,slug', 'faculties'])->where('slug', $slug)->first();
        if (empty($university)) abort(404);
        $page = request()->input('page', 1);

        if($request->filled('faculty') && $request->filled('dorm_type') && !empty($university->faculties)) {
            $faculty = $university->faculties->where('slug', $request->faculty)->first();
            if (!$faculty) abort(404);
            $gender = $request->dorm_type;
            $facultyDataWithGender = $this->facultyFilterWithGender($faculty->id, $gender, $page, $request->url(), $request->query());
            return response()->json($facultyDataWithGender, 200);
        }

        if($request->filled('faculty') && !empty($university->faculties)) {
            $faculty = $university->faculties->where('slug', $request->faculty)->first();
            if (!$faculty) abort(404);
            $facultyData = $this->facultyFilter($faculty->id, $page, $request->url(), $request->query());
            return response()->json($facultyData, 200);
        }

        if($request->filled('dorm_type')) {
            $universitiesData = $this->genderFilter($university, $request->dorm_type, $page, $request->url(), $request->query());
            return response()->json($universitiesData, 200);
        }

        $list = $this->list($university, $page, $request->url(), $request->query());
        return response()->json($list, 200);
    }

    public function list($university, $page, $url, $query)
    {
        $perPage = 40;

        $facultyIds = !empty($university->faculties) ? $university->faculties->pluck('id')->toArray() : [];

        $faculties = Faculty::with(['dorms' => function($q){
            $q->selectPublicFields();
        }, 'dorms.city:id,name,slug', 'dorms.district:id,name,slug'])->whereIn('id', $facultyIds)->get();

        $promotedDorms = $this->getPromotedDorms(['universityId' => $university->id]);

        $promote_ids = collect($promotedDorms)->pluck('id')->toArray();

        $normalDorms = $faculties->pluck('dorms')->collapse()->whereNotIn('id', $promote_ids)->shuffle()->toArray();

        $dorms = array_merge($promotedDorms, $normalDorms);

        $banners = $this->getBanners(['universityId' => $university->id]);

        $data = $this->mergeWithBanner($dorms, $banners);

        $popupDorms = $this->getPopupDorms(['universityId' => $university->id]);

        $dataWithPagination  =  new Paginator($data->forPage($page,$perPage), count($data), $perPage, $page, ['path' => $url, 'query' => $query]);

        return ['university' => $university->unsetRelation('faculties')->unsetRelation('dorms'), 'data' => $dataWithPagination, 'popups' => $popupDorms];
    }

    public function genderFilter($university, $gender, $page, $url, $query)
    {
        $perPage = 40;

        $facultyIds = $university->faculties->pluck('id')->toArray();

        $faculties = Faculty::with(['dorms' => function($q) use($gender){
            $q->selectPublicFields()->where('dorm_type_id', $gender);
        }, 'dorms.city:id,name,slug', 'dorms.district:id,name,slug'])->whereIn('id', $facultyIds)->get();

        $promotedDorms = $this->getPromotedDorms(['universityId' => $university->id, 'dormTypeId' => $gender]);

        $promote_ids = collect($promotedDorms)->pluck('id')->toArray();

        $normalDorms = $faculties->pluck('dorms')->collapse()->whereNotIn('id', $promote_ids)->shuffle()->toArray();

        $dorms = array_merge($promotedDorms, $normalDorms);

        $banners = $this->getBanners(['universityId' => $university->id, 'dormTypeId' => $gender]);

        $data = $this->mergeWithBanner($dorms, $banners);

        $popupDorms = $this->getPopupDorms(['universityId' => $university->id, 'dormTypeId' => $gender]);

        $dataWithPagination = new Paginator($data->forPage($page, $perPage), count($data), $perPage, $page, ['path' => $url, 'query' => $query]);

        return ['university' => $university->unsetRelation('faculties')->unsetRelation('dorms'), 'data' => $dataWithPagination, 'popups' => $popupDorms];
    }

    public function facultyFilter($facultyId, $page, $url, $query)
    {
        $perPage = 40;

        $faculty = Faculty::with(['dorms' => function($q){
            $q->selectPublicFields();
        }, 'dorms.city:id,name,slug', 'dorms.district:id,name,slug'])->where('id', $facultyId)->first();

        $promotedDorms = $this->getPromotedDorms(['facultyId' => $faculty->id]);

        $promote_ids = collect($promotedDorms)->pluck('id')->toArray();

        $normalDorms = $faculty->dorms->whereNotIn('id', $promote_ids)->shuffle()->toArray();

        $dorms = array_merge($promotedDorms, $normalDorms);

        $banners = $this->getBanners(['facultyId' => $faculty->id]);

        $data = $this->mergeWithBanner($dorms, $banners);

        $popupDorms = $this->getPopupDorms(['facultyId' => $faculty->id]);

        $dataWithPagination  =  new Paginator($data->forPage($page,$perPage), count($data), $perPage, $page, ['path' => $url, 'query' => $query]);

        return ['faculty' => $faculty->unsetRelation('dorms'), 'data' => $dataWithPagination, 'popups' => $popupDorms];

    }

    public function facultyFilterWithGender($facultyId, $gender, $page, $url, $query)
    {
        $perPage = 40;

        $faculty = Faculty::with(['dorms' => function($q) use($gender){
            $q->selectPublicFields()->where('dorm_type_id', $gender);
        }, 'dorms.city:id,name,slug', 'dorms.district:id,name,slug'], 'city')->where('id', $facultyId)->first();

        $promotedDorms = $this->getPromotedDorms(['facultyId' => $faculty->id, 'dormTypeId' => $gender]);

        $promote_ids = collect($promotedDorms)->pluck('id')->toArray();

        $normalDorms = $faculty->dorms->whereNotIn('id', $promote_ids)->shuffle()->toArray();

        $dorms = array_merge($promotedDorms, $normalDorms);

        $banners = $this->getBanners(['facultyId' => $faculty->id, 'dormTypeId' => $gender]);

        $data = $this->mergeWithBanner($dorms, $banners);

        $popupDorms = $this->getPopupDorms(['facultyId' => $faculty->id, 'dormTypeId' => $gender]);

        $dataWithPagination  =  new Paginator($data->forPage($page,$perPage), count($data), $perPage, $page, ['path' => $url, 'query' => $query]);

        return ['faculty' => $faculty->unsetRelation('dorms'), 'data' => $dataWithPagination, 'popups' => $popupDorms];
    }

    /**
     * @param array $opts
     * optional ['universityId', 'facultyId', 'dormTypeId']
     *
     * @return array
     */
    public function getPromotedDorms($opts) {
        $facultyId = $opts['facultyId'] ?? null;
        $dormTypeId = $opts['dormTypeId'] ?? null;

        $firstDorm = DP::where('promote_id', $this->getPromoteId('doping')) // doping = 3
        ->with(['dorm' => function($q){
            $q->selectPublicFields();
        }, 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type'])
            ->where([
                [ 'faculty_id', '=', $facultyId ],
                [ 'dorm_type_id', '=', $dormTypeId ]
            ]);
        if (!empty($opts['universityId'])) {
            $firstDorm = $firstDorm->where('university_id', $opts['universityId']);
        }
        $firstDorm = $firstDorm->inRandomOrder()->first();
        if($firstDorm) {
            $firstDorm->dorm->append('branch_count');
            $firstDorm->dorm['is_advert'] = 1;
            $firstDorm->dorm['promote_id'] = 3;
        }

        $listDorms = DP::where('promote_id', $this->getPromoteId('listeleme-sabit')) // listeleme-sabit = 4
        ->with(['dorm' => function($q){
            $q->selectPublicFields();
        }, 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type'])
            ->where([
                [ 'faculty_id', '=', $facultyId ],
                [ 'dorm_type_id', '=', $dormTypeId ]
            ]);
        if (!empty($opts['universityId'])) {
            $listDorms = $listDorms->where('university_id', $opts['universityId']);
        }
        $listDorms = $listDorms->orderBy('sort', 'ASC')->get();


        $listDorms2 = DP::where('promote_id', $this->getPromoteId('listeleme')) // listeleme = 5
        ->with(['dorm' => function($q){
            $q->selectPublicFields();
        }, 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type'])
            ->where([
                [ 'faculty_id', '=', $facultyId ],
                [ 'dorm_type_id', '=', $dormTypeId ]
            ]);
        if (!empty($opts['universityId'])) {
            $listDorms2 = $listDorms2->where('university_id', $opts['universityId']);
        }
        $listDorms2 = $listDorms2->inRandomOrder()->get();

        $listDormsData = [];
        foreach($listDorms as $row) {
            if (!empty($row->dorm)) {
                $row->dorm['promote_id'] = 4;
//                $row->dorm->append('branch_count');
                $listDormsData[] = $row->dorm->toArray();
            }
        }

        $listDormsData2 = [];
        foreach($listDorms2 as $row) {
            if (!empty($row->dorm)) {
                $row->dorm['promote_id'] = 5;
//                $row->dorm->append('branch_count');
                $listDormsData2[] = $row->dorm->toArray();
            }
        }

        $data = array_merge($firstDorm ? array($firstDorm->dorm->toArray()) : [],
            $listDormsData, $listDormsData2);
        return $data;
    }

    /**
     * @param array $opts
     * optional ['cityId', 'districtId', 'dormTypeId']
     *
     * @return array
     */
    public function getPopupDorms($opts) {
        $facultyId = $opts['facultyId'] ?? null;
        $dormTypeId = $opts['dormTypeId'] ?? null;
        $popupDorms = DP::where('promote_id', $this->getPromoteId('popup')) // popup = 6
        ->with('dorm:id,name,slug,center_slug,cover,city_id,user_id,district_id,meal,capacity,dorm_type_id', 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type')
            ->where([
                [ 'faculty_id', '=', $facultyId ],
                [ 'dorm_type_id', '=', $dormTypeId ]
            ]);
        if (!empty($opts['universityId'])) {
            $popupDorms = $popupDorms->where('university_id', $opts['universityId']);
        }
        $popupDorms = $popupDorms->orderBy('sort', 'ASC')->get();

        return $popupDorms;
    }

    /**
     * @param array $opts
     * optional ['cityId', 'districtId', 'dormTypeId']
     *
     * @return array
     */
    public function getBanners($opts) {
        $universityId = $opts['universityId'] ?? null;
        $facultyId = $opts['facultyId'] ?? null;
        $dormTypeId = $opts['dormTypeId'] ?? null;

        $banner = DP::where('promote_id', $this->getPromoteId('banner')) // banner = 9
        ->with(['dorm' => function($q){
            $q->selectPublicFields();
        }, 'dorm.city:id,slug,name', 'dorm.district:id,slug,name', 'dorm.type'])
            ->where([[ 'university_id', '=', $universityId ], [ 'faculty_id', '=', $facultyId ], [ 'dorm_type_id', '=', $dormTypeId ]])
            ->orderBy('sort', 'ASC')
            ->get()->toArray();

        return $banner;
    }

    public function mergeWithBanner($dorms, $banners) {
        $dorms = array_chunk($dorms, 8);
        $banners = array_chunk($banners, 2);
        foreach ($dorms as $key=>$item) {
            if (!empty($banners[0])) {
                array_push($dorms[$key], ['banners'=>$banners[0]]);
                array_shift($banners);
            }
        }
        $data = collect($dorms)->collapse();
        return $data;
    }

    public function getPromoteId($slug) {
        $banner = Promote::where('slug', $slug)->first();
        return $banner ? $banner->id : null;
    }

}
