<?php

namespace YurtlarBurada\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use YurtlarBurada\Http\Controllers\Controller;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Hash;
use YurtlarBurada\Models\Faculty;
use YurtlarBurada\Models\University;
use YurtlarBurada\Models\User;
use DB;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = ['email' => $request->email, 'password' => $request->password, 'is_active' => 1, 'type_id' => 2];
        if (Auth::guard('web')->attempt($credentials)) {
            $user = Auth::guard('web')->user();
//            // Send an internal API request to get an access token
//            $client = DB::table('oauth_clients')
//                ->where('password_client', true)
//                ->first();
//
//            // Make sure a Password Client exists in the DB
//            if (!$client) {
//                return response()->json([
//                    'message' => 'Laravel Passport is not setup properly.',
//                    'status' => 500
//                ], 500);
//            }
//
//            $data = [
//                'grant_type' => 'password',
//                'client_id' => $client->id,
//                'client_secret' => $client->secret,
//                'username' => $request->email,
//                'password' => $request->password,
//            ];
//            $oauthRequest = Request::create('/oauth/token', 'POST', $data);
//
//            $response = app()->handle($oauthRequest);
//            // Check if the request was successful
//            if ($response->getStatusCode() != 200) {
//                return response()->json([
//                    'message' => "Token does not create for ID : {$user->id} - Email: {$user->email} - {$response->getContent()}",
//                    'status' => 401
//                ], 401);
//            }
//
//            // Get the data from the response
//            $data = json_decode($response->getContent());

            $university = University::select('name')->find($user->university_id);
            $faculty = Faculty::select('name')->find($user->faculty_id);
            $user->university_name = $university->name ?? '';
            $user->faculty_name = $faculty->name ?? '';
            // Format the final response in a desirable format
            return response()->json([
                'token' => $user->createToken('Laravel Personal Access Client')->accessToken,
                'user' => $user,
                'status' => 200
            ]);
        }
        else {
            return response()->json([
                'message' => 'Wrong email or password!',
                'status' => 422
            ], 422);
        }

    }

    public function logout(Request $request)
    {
        $accessToken = auth()->user()->token();

        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        return response()->json(['status' => 200]);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|regex:/(0)[0-9]{10}/',
            'faculty_id' => 'required',
            'university_id' => 'required',
        ]);

        $table = new User;
        $table->type_id = 2;
        $table->first_name = $request->first_name;
        $table->last_name = $request->last_name;
        $table->university_id = $request->university_id;
        $table->faculty_id = $request->faculty_id;
        $table->phone = $request->phone;
        $table->email = $request->email;
        $table->password = bcrypt($request->password);
        $table->save();
        return response()->json($table, 201);
    }
}
