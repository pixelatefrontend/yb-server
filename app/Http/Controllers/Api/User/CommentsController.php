<?php

namespace YurtlarBurada\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;
use YurtlarBurada\Models\Comment;

class CommentsController extends Controller
{
    public function index()
    {
        $items = auth()->user()->load('comments.dorm:id,name,logo,city_id,district_id', 'comments.dorm.city:id,name', 'comments.dorm.district:id,name');
        return response()->json($items, 200);
    }
    
}
