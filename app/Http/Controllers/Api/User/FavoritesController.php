<?php

namespace YurtlarBurada\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;

class FavoritesController extends Controller
{
    public function index()
    {
        $items = auth()->user()->load('favorites.city:id,name,slug', 'favorites.district:id,name,slug');
        return response()->json($items, 200);
    }
}
