<?php

namespace YurtlarBurada\Http\Controllers\Api\User;

use YurtlarBurada\Models\User;
use Illuminate\Http\Request;
use YurtlarBurada\Http\Controllers\Controller;


class HomeController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        return response()->json($user, 200);
    }

    public function updatePersonalSettings()
    {
        $id = auth()->user()->id;
        $user = User::findOrFail($id);
        $user->first_name = request('first_name');
        $user->last_name = request('last_name');
        $user->email = request('email');
        $user->university_id = request('university');
        $user->faculty_id = request('faculty');
        if($user->save()) {
            return response()->json([], 200);
        }else {
            return response()->json([], 500);
        }
    }

    public function updateSessionSettings()
    {
        $this->validate(request(), [
            'password' => 'required|confirmed|min:6',
        ]);
        $id = auth()->user()->id;
        $user = User::findOrFail($id);
        $user->password = bcrypt(request('password'));
        if($user->save()) {
            return response()->json([], 200);
        }else {
            return response()->json([], 500);
        }
    }
}
