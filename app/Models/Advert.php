<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    public function category()
    {
        return $this->belongsTo(AdvertCategory::class, 'category_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function images()
    {
        return $this->hasMany(AdvertImage::class);
    }
}
