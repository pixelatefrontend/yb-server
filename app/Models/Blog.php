<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'category_id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function images()
    {
        return $this->hasMany(BlogImage::class);
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getCover()
    {
        return $this->icon;
    }

    public function getCoverAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/blogs/'.$this->id.'/'.$value;
        }
    }
}
