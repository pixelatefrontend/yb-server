<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class BlogImage extends Model
{
    public function getFilenameAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/blogs/'.$this->blog->id.'/'.$value;
        }
    }

    public function getImage()
    {
        return $this->filename;
    }

    public function blog()
    {
        return $this->belongsTo(Blog::class, 'blog_id');
    }
}
