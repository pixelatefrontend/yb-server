<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    public function getImageAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/cities/'.$this->id.'/'.$value;
        }
    }

    public function getIconAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/cities/'.$this->id.'/'.$value;
        }
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getIcon()
    {
        return $this->icon;
    }

    public function images()
    {
        return $this->hasMany(CityImage::class);
    }

    public function dorms()
    {
        return $this->hasMany(Dorm::class);
    }

    public function districts()
    {
        return $this->hasMany(District::class);
    }

    public function universities()
    {
        return $this->hasMany(University::class);
    }

    public function faculties()
    {
        return $this->hasMany(Faculty::class);
    }

    public function rooms()
    {
        return $this->hasManyThrough(DormRoom::class, Dorm::class);
    }
}
