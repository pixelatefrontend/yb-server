<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class CityComment extends Model
{
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
