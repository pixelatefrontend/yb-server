<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class CityImage extends Model
{
    public function getFilenameAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/cities/'.$this->city->id.'/'.$value;
        }
    }
    
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function getImage()
    {
        return $this->filename;
    }
}
