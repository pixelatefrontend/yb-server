<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{

    public function dorms()
    {
        return $this->hasMany(Dorm::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function universities()
    {
        return $this->hasMany(University::class);
    }

    public function rooms()
    {
        return $this->hasManyThrough(DormRoom::class, Dorm::class);
    }
}
