<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;
use YurtlarBurada\Models\Favorite;

/**
 * @method static findOrFail($id)
 */
class Dorm extends Model
{
    public $timestamps = true;
//    protected $appends = ['branch_count'];

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function scopeSelectPublicFields($query)
    {
        return $query->select('id', 'name', 'slug', 'logo', 'center_name', 'center_slug', 'cover', 'city_id', 'user_id', 'district_id', 'meal', 'capacity', 'dorm_type_id', 'is_wifi')->active();
    }

    public function getLogoAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/dorms/'.$this->id.'/'.$value;
        }
    }

    public function getCoverAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/dorms/'.$this->id.'/'.$value;
        }
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function getCover()
    {
        return $this->cover;
    }

    public function addView()
    {
        $this->increment('number_of_views');
    }

    // Relations

    public function properties()
    {
        return $this->belongsToMany(DormProperty::class, 'dorm_property_pivot', 'dorm_id', 'dorm_property_id');
    }

    public function city()
    {
    	return $this->belongsTo(City::class, 'city_id');
    }

    public function district()
    {
    	return $this->belongsTo(District::class, 'district_id');
    }

    public function type()
    {
    	return $this->belongsTo(DormType::class, 'dorm_type_id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function promotes()
    {
        return $this->belongsToMany(Promote::class)->withPivot('id','city_id','district_id','sort','faculty_id','top_title','bottom_title','column','color','dorm_type_id','start_date', 'end_date')->orderBy('sort', 'ASC');
    }

    public function promote()
    {
        return $this->hasMany(DormPromotePivot::class);
    }

    public function coverImage()
    {
        return $this->hasOne(DormImage::class, 'dorm_id')->latest();
    }

    public function universities()
    {
        return $this->belongsToMany(University::class);
    }

    public function faculties()
    {
        return $this->belongsToMany(Faculty::class);
    }

    public function images()
    {
        return $this->hasMany(DormImage::class);
    }

    public function video()
    {
        return $this->hasOne(DormVideo::class);
    }

    public function comments()
    {
        return $this->hasMany(DormComment::class);
    }

    public function clicks()
    {
        return $this->hasMany(DormPhoneClick::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(User::class, 'favorites', 'user_id', 'dorm_id')->withTimeStamps();
    }

    public function favorited()
    {
        return (bool) Favorite::where('user_id', auth()->user()->id)
                            ->where('dorm_id', $this->id)
                            ->first();
    }

    public function branches()
    {
        return $this->hasMany(Dorm::class, 'user_id', 'user_id');
    }

    public function getBranchCountAttribute()
    {
        $count = $this->branches()->where('id', '!=', $this->id)->where('user_id', '!=', '0')->count();
        return $count > 0 ? $count + 1 : $count;
    }

    public function rooms()
    {
        return $this->hasMany(DormRoom::class);
    }

}
