<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormComment extends Model
{
    public function replies()
    {
        return $this->hasMany(DormComment::class, 'parent_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function dorm()
    {
        return $this->belongsTo(Dorm::class, 'dorm_id');
    }
}
