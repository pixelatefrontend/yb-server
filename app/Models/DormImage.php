<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormImage extends Model
{
    public function getFilenameAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/dorms/'.$this->dorm->id.'/'.$value;
        }
    }

    public function getImage()
    {
        return $this->filename;
    }

    public function dorm()
    {
        return $this->belongsTo(Dorm::class, 'dorm_id')->select(['id']);
    }
}
