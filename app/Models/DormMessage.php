<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormMessage extends Model
{
    public function dorm()
    {
        return $this->belongsTo(Dorm::class, 'dorm_id');
    }
}
