<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormPhoneClick extends Model
{
    public $table = 'dorm_phone_clicks';
    public function dorm()
    {
        return $this->belongsTo(Dorm::class, 'dorm_id');
    }
}
