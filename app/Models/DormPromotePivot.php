<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormPromotePivot extends Model
{
    public $table = 'dorm_promote';

    public function promote()
    {
        return $this->belongsTo(Promote::class, 'promote_id');
    }

    public function dorm()
    {
        return $this->belongsTo(Dorm::class, 'dorm_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function university()
    {
        return $this->belongsTo(University::class, 'university_id');
    }

    public function faculty()
    {
        return $this->belongsTo(Faculty::class, 'faculty_id');
    }

    public function dormType() 
    {
        return $this->belongsTo(DormType::class, 'dorm_type_id');
    }
}
