<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DormProperty extends Model
{
    public $table = 'dorm_properties';
    
    public function category()
    {
        return $this->belongsTo(PropertyCategory::class, 'category_id');
    }

    public function dorms()
    {
        return $this->belongsToMany(Dorm::class, 'dorm_property_pivot', 'dorm_id', 'dorm_property_id')->withPivot('category_id');
    }

    public function getSlugAttribute($value)
    {
        return Str::slug($this->title);
    }
}
