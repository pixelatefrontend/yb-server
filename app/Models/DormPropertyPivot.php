<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormPropertyPivot extends Model
{
	public $timestamps = false;
	protected $table = 'dorm_property_pivot';
}
