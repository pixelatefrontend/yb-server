<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormReservation extends Model
{
    public function dorm()
    {
        return $this->belongsTo(Dorm::class, 'dorm_id');
    }
}
