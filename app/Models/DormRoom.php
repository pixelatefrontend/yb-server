<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormRoom extends Model
{
    public function dorm()
    {
        return $this->belongsTo(Dorm::class, 'dorm_id');
    }

    public function properties()
    {
        return $this->belongsToMany(DormProperty::class, 'dorm_property_pivot', 'dorm_id', 'dorm_property_id');
    }

    public function images()
    {
        return $this->hasMany(DormImage::class);
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function scopeSaleActive($query)
    {
        return $query->where('is_active', 1)->where('is_sale_active', 1);
    }
}
