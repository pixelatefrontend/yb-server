<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormRoomImage extends Model
{
    public function getFilenameAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/dorms/'.$this->dorm->id.'/'.$value;
        }
    }

    public function getImage()
    {
        return $this->filename;
    }

    public function dormRoom()
    {
        return $this->belongsTo(DormRoom::class, 'dorm_room_id')->select(['id']);
    }
}
