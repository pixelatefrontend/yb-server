<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class DormVideo extends Model
{
    protected $fillable = ["dorm_id", "url", "type", "width", "height", "is_active"];
    protected $primaryKey = 'dorm_id';
    public    $incrementing = false;

    public function dorm()
    {
        return $this->belongsTo(Dorm::class, 'dorm_id');
    }
}
