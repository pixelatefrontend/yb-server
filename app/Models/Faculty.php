<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    public function university()
    {
        return $this->belongsTo(University::class, 'university_id');
    }

    public function dorms()
    {
        return $this->belongsToMany(Dorm::class)->groupBy('dorm_id');
    }

    public function dormsCount()
    {
        return $this->belongsToMany(Dorm::class)->selectRaw('count(dorms.id) as aggregate')
        ->groupBy('dorm_id');
    }
}
