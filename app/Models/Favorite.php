<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    public function dorms()
    {
        return $this->belongsToMany(Dorm::class, 'favorites', 'user_id', 'dorm_id')->withTimeStamps();
    }
}
