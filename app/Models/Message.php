<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function dorm()
    {
    	return $this->belongsTo(Dorm::class);
    }

}
