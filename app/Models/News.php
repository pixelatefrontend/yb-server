<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table = 'news';
}
