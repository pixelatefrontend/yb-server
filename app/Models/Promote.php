<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class Promote extends Model
{
    public function dorms()
    {
        return $this->belongsToMany(Dorm::class)->withPivot('id','city_id','district_id','sort','faculty_id','top_title','bottom_title','column','color','dorm_type_id','start_date', 'end_date')->orderBy('sort', 'ASC');
    }
}
