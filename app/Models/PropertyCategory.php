<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyCategory extends Model
{
    public function properties()
    {
        return $this->hasMany(DormProperty::class, 'category_id');
    }
}
