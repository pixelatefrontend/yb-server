<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{

    public function getLogoAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/universities/'.$this->id.'/'.$value;
        }
    }


    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function faculties()
    {
        return $this->hasMany(Faculty::class);
    }

    public function dorms()
    {
        return $this->belongsToMany(Dorm::class)->groupBy('dorm_id');
    }

    public function dormsCount()
    {
        return $this->belongsToMany(Dorm::class)->count();
    }

    public function campuses()
    {
        return $this->hasMany(UniversityCampus::class);
    }

    public function images()
    {
        return $this->hasMany(UniversityImage::class);
    }
 }
