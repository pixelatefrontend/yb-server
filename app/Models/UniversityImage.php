<?php

namespace YurtlarBurada\Models;

use Illuminate\Database\Eloquent\Model;

class UniversityImage extends Model
{
    public function getFilenameAttribute($value)
    {
        if($value) {
            return env('DO_SPACES_CDN').'/uploads/universities/'.$this->university->id.'/'.$value;
        }
    }

    public function university()
    {
        return $this->belongsTo(University::class, 'university_id');
    }

    public function getImage()
    {
        return $this->filename;
    }
}
