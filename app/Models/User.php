<?php

namespace YurtlarBurada\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id', 'university_id', 'faculty_id', 'dorm_id', 'first_name', 'last_name', 'phone', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Relations

    public function type()
    {
        return $this->belongsTo(UserType::class, 'type_id');
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }

    public function faculty()
    {
        return $this->belongsTo(Faculty::class);
    }

    public function comments()
    {
        return $this->hasMany(DormComment::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(Dorm::class, 'favorites', 'user_id', 'dorm_id');
    }

    public function dorms()
    {
        return $this->hasMany('YurtlarBurada\Models\Dorm');
    }

    public function dorm()
    {
        return $this->belongsTo(Dorm::class);
    }

}
