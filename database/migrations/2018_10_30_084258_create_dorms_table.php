<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dorms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('old_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('dorm_type_id')->nullable();
            $table->integer('dorm_category_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('logo')->nullable();
            $table->string('cover')->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('cover_picture')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('phone_for_message')->nullable();
            $table->string('fax')->nullable();
            $table->text('address')->nullable();
            $table->string('member')->nullable();
            $table->string('web')->nullable();
            $table->string('email')->nullable();
            $table->longText('content')->nullable();
            $table->longText('content_en')->nullable();
            $table->longText('advantages')->nullable();
            $table->longText('events')->nullable();
            $table->longText('facilities')->nullable();
            $table->longText('transportation')->nullable();
            $table->longText('other')->nullable();
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->longText('virtual_tour')->nullable();
            $table->text('sketch')->nullable();
            $table->text('note')->nullable();
            $table->decimal('lng', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();
            $table->string('capacity')->nullable();
            $table->integer('number_of_likes')->nullable()->default(0);
            $table->integer('number_of_views')->nullable()->default(0);
            $table->integer('number_of_click')->nullable()->default(0);
            $table->integer('is_active')->nullable()->default(0);
            $table->tinyInteger('is_doping')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dorms');
    }
}
