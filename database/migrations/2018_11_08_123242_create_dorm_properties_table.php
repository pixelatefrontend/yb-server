<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDormPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dorm_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('title');
            $table->string('slug')->nullable();
            $table->integer('is_active')->default(1);
            $table->timestamps();
        });

        Schema::create('dorm_property_pivot', function(Blueprint $table) {
            $table->integer('dorm_id')->unsigned();
            $table->foreign('dorm_id')->references('id')->on('dorms')->onDelete('cascade');

            $table->integer('dorm_property_id')->unsigned();
            $table->foreign('dorm_property_id')->references('id')->on('dorm_properties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dorm_properties');
        Schema::dropIfExists('dorm_property_pivot');
    }
}
