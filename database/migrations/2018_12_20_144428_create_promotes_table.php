<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('dorm_promote', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dorm_id')->unsigned()->nullable();
            $table->foreign('dorm_id')->references('id')
                ->on('dorms')->onDelete('cascade');

            $table->integer('promote_id')->unsigned()->nullable();
            $table->foreign('promote_id')->references('id')
                ->on('promotes')->onDelete('cascade');
            $table->integer('city_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('university_id')->nullable();
            $table->integer('sort')->nullable();

            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotes');
        Schema::dropIfExists('dorm_promote');
    }
}
