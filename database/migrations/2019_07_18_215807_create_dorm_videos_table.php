<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDormVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dorm_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dorm_id');
            $table->string('type');
            $table->string('url');
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->integer('sort')->nullable();
            $table->integer('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dorm_videos');
    }
}
