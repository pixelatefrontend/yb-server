<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDormRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dorm_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dorm_id');
            $table->string('title');
            $table->integer('capacity');
            $table->integer('daily_price')->nullable();
            $table->integer('monthly_price')->nullable();
            $table->string('number')->nullable();
            $table->string('size')->nullable();
            $table->integer('sort')->nullable();
            $table->boolean('is_sale_active')->default(1);
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });

        Schema::create('dorm_rooms_property_pivot', function(Blueprint $table) {
            $table->integer('dorm_rooms_id')->unsigned();
            $table->foreign('dorm_rooms_id')->references('id')->on('dorm_rooms')->onDelete('cascade');

            $table->integer('dorm_property_id')->unsigned();
            $table->foreign('dorm_property_id')->references('id')->on('dorm_properties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dorm_rooms');
        Schema::dropIfExists('dorm_rooms_property_pivot');

    }
}
