<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDormRoomImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dorm_room_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dorm_room_id')->unsigned();
            $table->foreign('dorm_room_id')->references('id')->on('dorm_rooms')->onDelete('cascade');
            $table->string('filename');
            $table->integer('sort');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dorm_room_images');
    }
}
