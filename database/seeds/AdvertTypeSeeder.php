<?php

use Illuminate\Database\Seeder;
use App\Models\AdvertType as Type;
class AdvertTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::truncate();

        Type::create([
        	'title' => 'Banner',
        	'slug' => 'Banner'
        ]);

        Type::create([
        	'title' => 'Ana Sayfa Reklamları',
        	'slug' => 'ana-sayfa-reklamlari'
        ]);

        Type::create([
        	'title' => 'Vitrin Reklamları',
        	'slug' => 'vitrin-reklamlari'
        ]);

        Type::create([
        	'title' => 'Sponsor Reklamlar',
        	'slug' => 'sponsor-reklamlar'
        ]);

        Type::create([
        	'title' => 'İl Reklamları',
        	'slug' => 'il-reklamlari'
        ]);

        Type::create([
        	'title' => 'Üniversite Reklamları',
        	'slug' => 'universite-reklamlari'
        ]);

    }
}
