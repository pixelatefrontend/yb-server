<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;'); // Foreign keyler için kontrol 0
        $this->call(DormPropertiesSeeder::class);
        $this->call(PropertyCategoriesSeeder::class);
        $this->call(DormTypesSeeder::class);
        $this->call(UserTypeSeeder::class);
        $this->call(AdvertTypeSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
