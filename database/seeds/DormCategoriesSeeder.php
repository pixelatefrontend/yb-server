<?php

use Illuminate\Database\Seeder;
use App\Models\DormCategory;
class DormCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DormCategory::truncate();
        DormCategory::create([
            'title' => 'Meb\'e Bağlı Yurtlar',
            'slug' => 'meb-e-bagli-yurtlar'
        ]);
        DormCategory::create([
            'title' => 'Öğrenci Apartları',
            'slug' => 'ogrenci-apartlari',
        ]);
        DormCategory::create([
            'title' => 'Öğrenci Pansiyonları',
            'slug' => 'ogrenci-pansiyonlari'
        ]);
    }
}
