<?php

use Illuminate\Database\Seeder;
use YurtlarBurada\Models\DormProperty as Dorm;

class DormPropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;'); // Foreign keyler için kontrol 0
        Dorm::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        // Bina özellikleri id => 1
        Dorm::create([
            'category_id' => 1,
            'title' => 'Çamaşır Ütü Odası',
            'slug' => 'camasir-utu-odasi'
        ]);
        Dorm::create([
            'category_id' => 1,
            'title' => 'Etüt Odası',
            'slug' => 'etut-odasi'
        ]);
        Dorm::create([
            'category_id' => 1,
            'title' => 'Asasnsör',
            'slug' => 'asansor'
        ]);
        Dorm::create([
            'category_id' => 1,
            'title' => 'Kafeterya-Kantin',
            'slug' => 'kafeterya-kantin'
        ]);
        Dorm::create([
            'category_id' => 1,
            'title' => 'Odalarda Balkon',
            'slug' => 'odalarda-balkon'
        ]);
        Dorm::create([
            'category_id' => 1,
            'title' => 'Çizim Odası',
            'slug' => 'cizim-odasi'
        ]);
        Dorm::create([
            'category_id' => 1,
            'title' => 'Hoby, Oyun Odası',
            'slug' => 'hoby-oyun-odasi'
        ]);
        Dorm::create([
            'category_id' => 1,
            'title' => 'Spor Salonu',
            'slug' => 'spor-salonu'
        ]);
        // Oda Özellikleri id => 2
        Dorm::create([
            'category_id' => 2,
            'title' => 'Oda içi Mobilya (Mini buzdolabı, Yatak, Kişisel Dolap ve Çalışma Masası)',
            'slug' => 'oda_ici_mobilya'
        ]);
        Dorm::create([
            'category_id' => 2,
            'title' => 'Klima',
            'slug' => 'klima'
        ]);
        Dorm::create([
            'category_id' => 2,
            'title' => 'Banyo-WC',
            'slug' => 'banyo-wc'
        ]);
        Dorm::create([
            'category_id' => 2,
            'title' => 'LCD TV-Uydu',
            'slug' => 'lcd-tv-uydu'
        ]);
        Dorm::create([
            'category_id' => 2,
            'title' => 'Wi-Fi',
            'slug' => 'wi-fi'
        ]);
        // Hizmetler id => 3
        Dorm::create([
            'category_id' => 3,
            'title' => 'Sabah Kahvaltısı',
            'slug' => 'sabah-kahvaltisi',
        ]);
        Dorm::create([
            'category_id' => 3,
            'title' => 'Temizlik (2 günde 1 yada haftada 1)',
            'slug' => 'temizlik',
        ]);
        Dorm::create([
            'category_id' => 3,
            'title' => 'Güvenli Bina Girişi',
            'slug' => 'guvenli-bina-girisi',
        ]);
        Dorm::create([
            'category_id' => 3,
            'title' => 'Güvenlik Gözetleme Kamera Sistemleri',
            'slug' => 'guvenlik-gozetleme-kamera-sistemleri',
        ]);
        Dorm::create([
            'category_id' => 3,
            'title' => 'Lig TV',
            'slug' => 'lig-tv',
        ]);
        Dorm::create([
            'category_id' => 3,
            'title' => 'Hoby Alanı',
            'slug' => 'hoby-alani',
        ]);
        Dorm::create([
            'category_id' => 3,
            'title' => 'Hoby Mutfak',
            'slug' => 'hoby-mutfak',
        ]);
        Dorm::create([
            'category_id' => 3,
            'title' => 'Ücretsiz Servis',
            'slug' => 'ucretsiz-servis',
        ]);
    }
}
