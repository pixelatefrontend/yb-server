<?php

use Illuminate\Database\Seeder;
use App\Models\DormType;
class DormTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DormType::truncate();
        DormType::create([
            'title' => 'Erkek Öğrenci Yurdu',
            'slug' => 'erkek-ogrenci-yurdu'
        ]);
        DormType::create([
            'title' => 'Kız Öğrenci Yurdu',
            'slug' => 'kiz-ogrenci-yurdu'
        ]);
    }
}
