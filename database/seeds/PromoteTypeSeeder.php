<?php

use Illuminate\Database\Seeder;
use App\Models\Promote;

class PromoteTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Promote::truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        Promote::create([
        	'name' => 'Ana Sayfa (Slider)',
        	'slug' => 'ana-sayfa'
        ]);
        Promote::create([
        	'name' => 'Ana Sayfa Vitrin',
        	'slug' => 'vitrin'
        ]);
        Promote::create([
        	'name' => '1. Sıra Sabit',
        	'slug' => 'doping'
        ]);
        Promote::create([
        	'name' => 'Listeleme',
        	'slug' => 'listeleme'
        ]);
        Promote::create([
        	'name' => 'Banner',
        	'slug' => 'banner'
        ]);
        Promote::create([
        	'name' => 'Popup',
        	'slug' => 'popup'
        ]);
        Promote::create([
        	'name' => 'Blog Listeleme',
        	'slug' => 'blog-listeleme'
        ]);
        Promote::create([
        	'name' => 'Blog Detay',
        	'slug' => 'blog-detay'
        ]);
    }
}
