<?php

use Illuminate\Database\Seeder;
use App\Models\PropertyCategory as Property;

class PropertyCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Property::truncate();
        Property::create([
            'title' => 'Bina Özellikleri'
        ]);

        Property::create([
            'title' => 'Oda Özellikleri'
        ]);

        Property::create([
            'title' => 'Hizmetler'
        ]);
        //
    }
}
