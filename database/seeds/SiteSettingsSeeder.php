<?php

use Illuminate\Database\Seeder;
use App\Models\SiteSetting as SS;
class SiteSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SS::truncate();
        SS::create([
            'title' => 'Yurtlar Burada',
            'description' => 'Yurtlar Burada Site Açıklaması',
            'keywords' => 'yurtlar, erkek öğrenci yurdu, kız öğrenci yurdu yurtlar burada',
            'about' => 'Hakkımızda Yazısı',
            'member' => 'John Doe',
            'land_phone' => '',
            'mobile_phone' => '',
            'fax' => '',
            'email' => 'info@yurtlarburada.com',
            'city_list_seo' => '',
            'university_list_seo' => '',
            'facebook' => '',
            'twitter' => '',
            'instagram' => '',
            'linkedin' => '',
        ]);
    }
}
