<?php

use Illuminate\Database\Seeder;
use App\Models\UserType as Type;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::truncate();

        Type::create([
        	'type' => 'Yurt',
        	'slug' => 'yurt'
        ]);
        
        Type::create([
        	'type' => 'Öğrenci',
        	'slug' => 'ogrenci'
        ]);
    }
}
