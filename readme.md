# Install

    composer install

### Usage
    docker-compose up
    
    
create .env

```cp .env.example .env```

Update .env with your environments

Visit: http://localhost:8090

You can change port on `docker-compose.yml`
    
### Development 
    docker-compose exec php php artisan migrate
docker-compose exec php \<PHP COMMANDS> (eg: php artisan list)

#### Mysql
    docker-compose exec mysql mysql -u root -proot

Getting database information from .env
```
...
   DB_CONNECTION=mysql
   DB_HOST=mysql
   DB_PORT=3306
   DB_DATABASE=dorm_db
   DB_USERNAME=user
   DB_PASSWORD=secret
...
```
