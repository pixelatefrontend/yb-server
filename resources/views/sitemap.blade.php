<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<sitemap>
        <loc>{{ route('sitemap.cities') }}</loc>
        <lastmod>{{ $now }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ route('sitemap.districts') }}</loc>
        <lastmod>{{ $now }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ route('sitemap.universities') }}</loc>
        <lastmod>{{ $now }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ route('sitemap.faculties') }}</loc>
        <lastmod>{{ $now }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ route('sitemap.dorms') }}</loc>
        <lastmod>{{ $now }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ route('sitemap.blogs') }}</loc>
        <lastmod>{{ $now }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ route('sitemap.static') }}</loc>
        <lastmod>{{ $now }}</lastmod>
    </sitemap>
</sitemapindex>
