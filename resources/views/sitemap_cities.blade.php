<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">

    <url>
        <loc>https://www.yurtlarburada.com/</loc>
        <lastmod>{{ $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>0.8</priority>
    </url>

    @foreach($cities as $city)
    <url>
        <loc>https://www.yurtlarburada.com/{{ $city->slug }}-ozel-ogrenci-yurtlari/</loc>
        <lastmod>{{ $city->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>1.0</priority>
    </url>
    @endforeach

    @foreach($cities as $city)
    <url>
        <loc>https://www.yurtlarburada.com/{{ $city->slug }}-kiz-ogrenci-yurtlari/</loc>
        <lastmod>{{ $city->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>1.0</priority>
    </url>
    @endforeach

    @foreach($cities as $city)
    <url>
        <loc>https://www.yurtlarburada.com/{{ $city->slug }}-erkek-ogrenci-yurtlari/</loc>
        <lastmod>{{ $city->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>1.0</priority>
    </url>
    @endforeach

    @foreach($cities as $city)
    <url>
        <loc>https://www.yurtlarburada.com/{{ $city->slug }}-sehri-hakkinda/</loc>
        <lastmod>{{ $city->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>0.6</priority>
    </url>
    @endforeach

</urlset>
