<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">

    <url>
        <loc>https://www.yurtlarburada.com/</loc>
        <lastmod>{{ $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>0.8</priority>
    </url>

    @foreach($districts as $district)
    <url>
        @if($district->city)
            <loc>https://www.yurtlarburada.com/{{ $district->city->slug }}-{{ $district->slug }}-ilcesi-ogrenci-yurtlari/</loc>
        @endif
        <lastmod>{{ $district->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>1.0</priority>
    </url>
    @endforeach

    @foreach($districts as $district)
    <url>
        @if($district->city)
            <loc>https://www.yurtlarburada.com/{{ $district->city->slug }}-{{ $district->slug }}-ilcesi-kiz-ogrenci-yurtlari/</loc>
        @endif
        <lastmod>{{ $district->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>1.0</priority>
    </url>
    @endforeach

    @foreach($districts as $district)
    <url>
        @if($district->city)
            <loc>https://www.yurtlarburada.com/{{ $district->city->slug }}-{{ $district->slug }}-ilcesi-erkek-ogrenci-yurtlari/</loc>
        @endif
        <lastmod>{{ $district->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>1.0</priority>
    </url>
    @endforeach

</urlset>
