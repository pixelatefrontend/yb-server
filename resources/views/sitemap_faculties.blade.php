<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">

    <url>
        <loc>https://www.yurtlarburada.com/</loc>
        <lastmod>{{ $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>0.8</priority>
    </url>

    @foreach($faculties as $faculty)
    <url>
        @if($faculty->university)
            <loc>https://www.yurtlarburada.com/{{ $faculty->university->slug }}--{{ $faculty->slug }}-ogrenci-yurtlari/</loc>
        @endif
        <lastmod>{{ $faculty->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>0.8</priority>
    </url>
    @endforeach

    @foreach($faculties as $faculty)
    <url>
        @if($faculty->university)
            <loc>https://www.yurtlarburada.com/{{ $faculty->university->slug }}--{{ $faculty->slug }}--kiz-ogrenci-yurtlari/</loc>
        @endif
        <lastmod>{{ $faculty->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>0.8</priority>
    </url>
    @endforeach

    @foreach($faculties as $faculty)
    <url>
        @if($faculty->university)
            <loc>https://www.yurtlarburada.com/{{ $faculty->university->slug }}--{{ $faculty->slug }}--erkek-ogrenci-yurtlari/</loc>
        @endif
        <lastmod>{{ $faculty->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>0.8</priority>
    </url>
    @endforeach

</urlset>
