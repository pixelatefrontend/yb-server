<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">

    <url>
        <loc>https://www.yurtlarburada.com/</loc>
        <lastmod>{{ $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>0.8</priority>
    </url>

    @foreach($universities as $university)
    <url>
        <loc>https://www.yurtlarburada.com/{{ $university->slug }}-yakin-ogrenci-yurtlari/</loc>
        <lastmod>{{ $university->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>1.0</priority>
    </url>
    @endforeach

    @foreach($universities as $university)
    <url>
        <loc>https://www.yurtlarburada.com/{{ $university->slug }}-yakin-kiz-ogrenci-yurtlari/</loc>
        <lastmod>{{ $university->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>1.0</priority>
    </url>
    @endforeach

    @foreach($universities as $university)
    <url>
        <loc>https://www.yurtlarburada.com/{{ $university->slug }}-yakin-erkek-ogrenci-yurtlari/</loc>
        <lastmod>{{ $university->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>1.0</priority>
    </url>
    @endforeach

    @foreach($universities as $university)
    <url>
        <loc>https://www.yurtlarburada.com/{{ $university->slug }}-hakkinda/</loc>
        <lastmod>{{ $university->updated_at ?? $now }}</lastmod>
        <changefreq>Daily</changefreq>
        <priority>0.8</priority>
    </url>
    @endforeach

</urlset>
