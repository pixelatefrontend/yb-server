<?php
use Illuminate\Http\Request;
use YurtlarBurada\Models\University;
use YurtlarBurada\Models\Faculty;
use YurtlarBurada\Models\DormPromotePivot as DP;

Route::get('/selected/universities', function(){
    $universities = University::select('id', 'name')->get();
    return response()->json($universities, 200);
});

Route::get('/selected/faculties/{university_id}', function($university_id) {
    $faculties = Faculty::select('id', 'university_id', 'name')->where('university_id', $university_id)->get();
    return response()->json($faculties, 200);
});

Route::group([ 'namespace' => 'Api\Panel', 'prefix' => 'panel'], function(){
    Route::group([ 'middleware' => 'auth:api' ], function() {

        Route::get('user_info', function (Request $request) {
            return $request->user();
        });

        // Dashboard
        Route::get('/', 'DashboardController@index');
        // Yurt Yönetimi
        Route::resource('dorms', 'DormsController');
        Route::post('dorm/image-destroy/{id}', 'DormsController@imageDestroy');
        Route::post('dorm/logo-destroy/{id}', 'DormsController@logoDestroy');
        Route::post('dorm/cover-destroy/{id}', 'DormsController@coverDestroy');
        // Kullanıcılar
        Route::resource('users', 'UsersController');
        // Reklam Yönetimi
        Route::resource('promotes', 'PromotesController');
        // Üniversiteler
        Route::resource('universities', 'UniversitiesController');
        Route::post('university/image-destroy/{id}', 'UniversitiesController@imageDestroy');
        // Fakülteler
        Route::resource('faculties', 'FacultiesController');
        Route::get('search/faculties/{university_id}', 'FacultiesController@getFaculties');
        // İlan Yönetimi
        Route::resource('adverts', 'AdvertsController');
        // Haber Yönetimi
        Route::resource('news', 'NewsController');
        // Sayfa Yönetimi
        Route::resource('pages', 'PagesController');
        // Blog Yönetimi
        Route::resource('blogs', 'BlogsController');
        Route::post('blog/cover-destroy/{id}', 'BlogsController@coverDestroy');
        Route::post('blog/image-destroy/{id}', 'BlogsController@imageDestroy');
        // Blog Kategori Yönetimi
        Route::resource('blog_categories', 'BlogCategoriesController');
        // İletişim Formu
        Route::resource('contacts', 'ContactsController');
        Route::get('contacts/{id}/is_read', 'ContactsController@getRead');
        // Mesajlar
        Route::resource('messages', 'MessagesController');
        // İl ve ilçe
        Route::resource('cities', 'CitiesController');
        Route::post('city/cover-destroy/{id}', 'CitiesController@coverDestroy');
        Route::post('city/icon-destroy/{id}', 'CitiesController@iconDestroy');
        Route::post('city/image-destroy/{id}', 'CitiesController@imageDestroy');
        Route::get('districts/{city_id}', 'LocationsController@getDistricts');
        Route::resource('districts', 'DistrictsController');
        // Yurt Özellikleri
        Route::get('dorm_properties/list', 'DormPropertiesController@getList');
        Route::resource('dorm_properties', 'DormPropertiesController');
        // Yurt Kategorileri
        Route::resource('dorm_categories', 'DormCategoriesController');
        // Yurt Odalari
        Route::resource('dorm_rooms', 'DormRoomsController');
        Route::post('dorm_room/image-destroy/{id}', 'DormRoomsController@imageDestroy');
        // Site Ayarları
        Route::get('settings', 'SiteSettingsController@getData');
        Route::post('settings/{id}', 'SiteSettingsController@postData');
        // Panel Kullanıcıları
        Route::resource('panel_users', 'PanelUsersController');
        // Session sonlandır
        Route::post('logout', 'AuthController@logout')->name('logout');

    });

    // Panel Authentication
    Route::post('/login', 'AuthController@login')->name('panel.login');

});

// Site API Routeları
// Controller Path = App/Http/Controllers/Api/Site/..
// Domain = env('API_URL).'/site/'
Route::group([ 'namespace' => 'Api\Site', 'prefix' => 'site' ], function(){

    Route::get('sitemap', 'SitemapController@index');
    Route::get('sitemap/cities', 'SitemapController@cities')->name('sitemap.cities');
    Route::get('sitemap/districts', 'SitemapController@districts')->name('sitemap.districts');
    Route::get('sitemap/universities', 'SitemapController@universities')->name('sitemap.universities');
    Route::get('sitemap/faculties', 'SitemapController@faculties')->name('sitemap.faculties');
    Route::get('sitemap/dorms', 'SitemapController@dorms')->name('sitemap.dorms');
    Route::get('sitemap/blogs', 'SitemapController@blogs')->name('sitemap.blogs');
    Route::get('sitemap/static', 'SitemapController@static')->name('sitemap.static');

    Route::get('dorm-list-all', function(){
        $data = DP::with('dorm:id,name')->get();
        return response()->json($data, 200);
    });

    // Site Genel Ayarları
    Route::get('settings', 'SettingsController@getSettings');
    Route::get('counter', 'SettingsController@getCounter');
    // Ana Sayfa
    Route::get('home/slider-promote-dorms', 'HomeController@sliderPromoteDorms'); // Slider reklamlarını gösterecek
    Route::get('home/showcase-promote-dorms', 'HomeController@showCasePromoteDorms'); // Vitrin reklamlarını gösterecek
    Route::get('home/select-cities', 'HomeController@selectCities'); // Arama bölümündeki selectbox içinde şehirleri listeleyecek
    Route::get('home/universities/{city_id}', 'HomeController@universities'); // Arama bölümündeki selectboxun içindeki üniversiteleri seçilen şehire göre getirecek
    Route::get('home/cities', 'HomeController@cities'); // En çok yurt olan şehirleri listeleyecek (Şehir sayısı opsiyoneldir. default olarak 5 ayarlandı.)
    Route::get('home/blogs', 'HomeController@blogs'); // Öne çıkarılan blog yazılarını listeleyecek
    Route::get('search', 'HomeController@search');
    // Şehir
    Route::get('cities/cities', 'CitiesController@cities'); // Şehirleri listeleyecek plaka koduna göre
    Route::get('cities/popular', 'CitiesController@popularCities'); // Popüler şehirleri listeleyecek (Yurdu en fazla olan şehire göre)
    Route::get('cities/dorms/{slug}', 'CitiesController@dorms'); // Şehir'in slug'ına göre yurtları listeleyecek
    Route::get('cities/{slug}', 'CitiesController@detail'); // Şehir detay bilgisi
    // Universite
    Route::get('universities/list', 'UniversitiesController@universities'); // Üniversiteleri listeleyecek
    Route::get('universities/detail/{slug}', 'UniversitiesController@detail'); // Üniversite detay bilgisi
    Route::get('universities/dorms/{slug}', 'UniversitiesController@dorms'); // Üniversiteye yakın olan yurtları listeleyecek
    Route::get('universities/university/{city_id}', 'UniversitiesController@filterUniversity'); // Şehire göre üniversite filtreleme
    // Blog
    Route::get('blogs/list', 'BlogsController@list'); // Blog listeleme
    Route::get('blogs/detail/{id}', 'BlogsController@detail'); // Blog detay
    Route::get('blogs/category/list', 'BlogsController@categories'); // Blog kategori listesi
    Route::get('blogs/category/{slug}', 'BlogsController@category'); // Kategoriye ait blogları getirecek
    // Adverts
    Route::get('adverts/list', 'AdvertsController@list'); // İlan listeleme
    Route::get('adverts/detail/{slug}--{id}', 'AdvertsController@detail'); // İlan detay
    Route::get('adverts/category/list', 'AdvertsController@categories'); // İlan kategori listesi
    Route::get('adverts/category/{slug}', 'AdvertsController@category'); // Kategoriye air ilanları getirecek
    // Yurtlar
    Route::get('dorm/detail/{id}', 'DormsController@detail')->name('dormDetail'); // Yurt detay
    Route::get('dorm/detail/old/{old_id}', 'DormsController@detailByOldId'); // Yurt detay
    Route::get('dorm/list', 'DormsController@list'); // Tüm yurtları listeleyecek
    Route::post('dorm/view_number_button/{id}', 'DormsController@viewNumberButton'); // Telefon Numarasını Göster butonunun tıklanma saysını tutacak
    Route::post('dorm/comment/{dorm_id}', 'DormsController@postComment'); // Yurt yorum yapma işlemleri
    Route::post('dorm/message/multiple', 'DormsController@postMultipleMessage'); // Yurt mesaj gönderme işlemler
    Route::post('dorm/message/{dorm_id}', 'DormsController@postMessage'); // Yurt mesaj gönderme işlemler
    Route::post('dorm/reservation/{dorm_id}', 'DormsController@postReservation'); // Yurt oda rezervasyon işlemleri
    Route::post('dorm/favorite/{dorm_id}', 'DormsController@addFavorite'); // Yurt favoriye ekleme
    Route::post('dorm/unfavorite/{dorm_id}', 'DormsController@destroyFavorite'); // Yurt favoriden çıkarma
    Route::post('dorm/pre_register', 'DormsController@postPreRegister'); // Yurt mesaj gönderme işlemler
    Route::get('dorm/branches/{dorm_id}', 'DormsController@getBranches'); // Yurt detay
    // Search
    Route::get('search/dorms', 'SearchController@getList'); // Yurt Adına Göre ara sayfasına gönderilecek tüm yurtlar
    // İletişim Formu
    Route::post('contact', 'ContactController@postMessage');
    // Yurt Odalari Listeleme
    Route::get('districts/rooms', 'DormRoomsController@getDistrictsOfRooms');
    Route::get('rooms', 'DormRoomsController@getCitiesOfRooms');
    Route::get('rooms/city/{slug}', 'DormRoomsController@getFilteredRooms');
    Route::get('rooms/dorm/{dorm_id}', 'DormRoomsController@getRoomsOfDorm');
});

Route::group([ 'namespace' =>  'Api\Dorm', 'prefix' => 'dorm' ], function(){
    Route::group([ 'middleware' => 'auth:api' ], function() {
        // Giriş Bilgileri
        Route::get('info', 'InfoController@get');
        Route::put('info', 'InfoController@updateInfo');
        Route::put('password', 'InfoController@updatePassword');
        // Yurtlarım
        Route::get('dorms', 'DormsController@list');
        Route::get('dorms/{dorm_id}', 'DormsController@get');
        Route::post('dorms', 'DormsController@store');
        Route::put('dorms/{dorm_id}', 'DormsController@update');
        // İstatistikler
        Route::get('statistics', 'StatisticsController@getStatistics');
        Route::get('statistics/{dorm_id}', 'StatisticsController@getDormStatistics');
        // Mesajlar
        Route::get('messages', 'MessagesController@list');
        Route::get('message/{message_id}', 'MessageController@detail');
        // Yorumlar
        Route::get('comments', 'CommentsController@list');
        Route::get('comment/{comment_id}', 'CommentsController@detail');
        // İlanlar
        Route::get('adverts', 'AdvertsController@list');
        Route::get('advert/{advert_id}', 'AdvertsController@detail');
        Route::post('advert/create', 'AdvertsController@create');
        Route::put('advert/{advert_id}/update', 'AdvertsController@update');
    });
    // Middleware auth
    Route::post('/login', 'AuthController@login')->name('dorm.login'); //
    Route::post('/register', 'AuthController@register');

});

Route::group([ 'namespace' => 'Api\User', 'prefix' => 'user' ], function() {

    Route::group([ 'middleware' => 'auth:api' ], function() {
        Route::get('info', function(Request $request) {
            return $request->user();
        });
        Route::get('/', 'HomeController@index'); // Kullanıcı paneli dashboard
        Route::get('offers', 'OffersController@index'); // Kullanıcı paneli teklifler
        Route::get('comments', 'CommentsController@index'); // Kullanıcı paneli yorumlar
        Route::get('favorites', 'FavoritesController@index'); // Kullanıcı paneli favori yurtlar
        // Kullanıcı Bilgi Güncelleme
        Route::put('update/personal', 'HomeController@updatePersonalSettings');
        Route::put('update/session', 'HomeController@updateSessionSettings');
        // Session sonlandır
        Route::post('logout', 'AuthController@logout')->name('logout');
    });
    // Middleware auth
    Route::post('/login', 'AuthController@login')->name('user.login'); // Kullanıcı giriş
    Route::post('/register', 'AuthController@register'); // Kullanıcı kayıt ol
});
